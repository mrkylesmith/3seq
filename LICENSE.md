This work is licensed under the **Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License**.

In short: you are free to share and make derivative works of the work under the conditions that you appropriately attribute it, you use the material only for non-commercial purposes, and that you distribute it only under a license compatible with this one.