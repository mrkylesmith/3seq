//
// Created by lamhm on 16/02/2022.
//

#ifndef INC_3SEQ_ALIGNMENT_H
#define INC_3SEQ_ALIGNMENT_H

#include <vector>
#include <set>

#include "Sequence.h"
#include "AllelicMask.h"
#include "SequencePool.h"


class AlignmentDescriptor;


class Alignment {
public:
    friend class AlignmentDescriptor;

    // Forbid all move and copy constructors
    Alignment(const Alignment &rhs) = delete;

    Alignment &operator=(const Alignment &) = delete;

    Alignment(Alignment &&) = delete;

    Alignment &operator=(Alignment &&) = delete;

    Alignment();

    /**
     * Add a sequence to the alignment. The sequence will be added to both the child and the parent
     * pools.
     * @param sequencePtr A pointer to the sequence.
     * @param asParent A flag that indicates if the sequence should be added to the parent pool.
     * @param asChild  A flag that indicates if the sequence should be added to the child pool.
     */
    void addSequence(const SequencePtr &sequencePtr, const bool &asParent, const bool &asChild);


    void addSequences(const std::vector<SequencePtr> &sequenceList,
                      const bool &asParent, const bool &asChild);


    unsigned long activeLength() const;

    /**
     * Get the original number of columns of the alignment as read from the sequence file
     * (before shrinking, cutting)
     * @return The original number of columns of the alignment
     */
    unsigned long fullLength() const;

    bool isChildParentFromSamePool() const;

    /**
     * Lock the alignment, i.e. forbidding adding more sequences into the alignment.
     * The locking process also does: [1] populating the active nucleotide-position vector
     * and [2] scan and mark the allelic status of each alignment column.
     */
    void lock();

    /**
     * Delete all BUT the columns between the <b>beginPos</b> and the <b>endPos</b>.
     * @param beginPos
     * @param endPos
     * @param isOneBasedIdx Indicates if the indexing of the <i>beginPos</i> and <i>endPos</i>
     *                      is 1-based. If not, the positions are considered 0-based.
     * @note The positions of the columns are the original indices that are stored in the
     *       <b>activePositions</b> vector.
     */
    void shrink(unsigned long beginPos, unsigned long endPos, const bool &isOneBasedIdx);

    /**
     * Delete all columns that are between the <b>beginPos</b> and the <b>endPos</b> of the
     * alignment.
     * @param beginPos
     * @param endPos
     * @param isOneBasedIdx Indicates if the indexing of the <i>beginPos</i> and <i>endPos</i>
     *                      is 1-based. If not, the positions are considered 0-based.
     * @note The positions of the columns are the original indices that are stored in the
     *       <b>activePositions</b> vector.
     */
    void cutOut(unsigned long beginPos, unsigned long endPos, const bool &isOneBasedIdx);

    /**
     * Delete specific codon positions.
     * @param first   Indicates if all first-codon positions should be deleted.
     * @param second  Indicates if all second-codon positions should be deleted.
     * @param third   Indicates if all third-codon positions should be deleted.
     * @param useOrigColumnIdxs  Indicates if the original column indices (as read from the sequence
     *                           input file) should be used to calculate the codon positions of the
     *                           columns. If this is set to <i>false</i>, the derived indices of the
     *                           columns (after the alignment has been shrunk and/or cut) will be
     *                           used for the calculation.
     */
    void excludeCodonPositions(const bool &first,
                               const bool &second,
                               const bool &third,
                               const bool &useOrigColumnIdxs = false);

    /**
     * Remove all non-polymorphic columns from the alignment.
     */
    void excludeMonomorphicColumns();


    /**
     * Get the number of nucleotide positions of the genome that are covered by this alignment.
     * This number is calculated using the formula:
     * <b>rightmost_position - leftmost_position + 1</b>, where <b>leftmost_position</b> and
     * <b>rightmost_position</b> are the minimum and maximum nucleotide positions, respectively,
     * that are kept on the alignment. Note that the count also includes already-removed columns
     * whose positions are between the <b>leftmost_position</b> and <b>rightmost_position</b>.
     * @return The number of nucleotide positions of the genome that are covered by this alignment.
     */
    unsigned long nCoveredNuPositions() const;

    const std::vector<SequencePtr> &getActiveParents() const;

    const std::vector<SequencePtr> &getActiveChildren() const;

    SequencePool &getParentPool();

    SequencePool &getChildPool();

    std::set<SequencePtr> getAllUsedSequences() const;

    unsigned long countAllSequences() const;

    unsigned long countUsedSequences() const;

    unsigned long countDistinctSequences() const;

private:
    /**
     * A pointer to the vector that contains active nucleotide-positions.
     * The vector is managed by the Alignment class.
     */
    std::shared_ptr<std::vector<unsigned long>> activeNuPositions;

    /**
     * The vector contains pointers to all sequences in this alignment,
     * including both parent and child sequences.
     */
//    SequencePool masterPool;

    /**
     * A subset of sequences from the <b>masterPool</b> vector.
     * These sequences will be used as parents in the analysis.
     */
    SequencePool parentPool;

    /**
     * A subset of sequences from the <b>masterPool</b> vector.
     * These sequences will be used as children in the analysis.
     */
    SequencePool childPool;

    unsigned long fullSequenceLength;

    /** The vector contains allelic status of each column in the alignment */
    std::vector<AllelicMask> allelicMarkers;

    /**
     * A flag that indicates if the alignment has been locked or it is still open for adding more
     * sequences. The alignment will be locked (i.e. no more sequences can be added into it) as soon
     * as the processing of the alignment (e.g. trimming, cutting) starts.
     */
    bool locked;

    /**
     * Whether parent and child sequences come from the same sequence pool.
     */
    bool isSinglePool;

    void populatePositionVector();

    void markAllelicStatuses();

};


#endif //INC_3SEQ_ALIGNMENT_H
