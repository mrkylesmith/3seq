//
// Created by lamhm on 04/05/22.
//

#ifndef INC_3SEQ_BREAKPOINT_H
#define INC_3SEQ_BREAKPOINT_H

#include <memory>
#include <utility>
#include <string>
#include <sstream>


class BreakPoint {
public:
    unsigned long leftBound;
    unsigned long rightBound;
    long long height;

    explicit BreakPoint(const unsigned long &leftBound,
                        const unsigned long &rightBound,
                        const long long &height)
            : leftBound(leftBound), rightBound(rightBound), height(height) {};

    static std::shared_ptr<BreakPoint> create(const unsigned long &leftBound,
                                              const unsigned long &rightBound,
                                              const long long &height) {
        return std::make_shared<BreakPoint>(leftBound, rightBound, height);
    };
};


typedef std::shared_ptr<BreakPoint> BreakPointPtr;


class BreakPointPair {
public:
    BreakPointPtr leftBreakPoint;
    BreakPointPtr rightBreakPoint;

    explicit BreakPointPair(BreakPointPtr leftBreakPoint,
                            BreakPointPtr rightBreakPoint)
            : leftBreakPoint(std::move(leftBreakPoint)),
              rightBreakPoint(std::move(rightBreakPoint)) {};

    std::string toString() const {
        std::stringstream strStream;
        strStream << leftBreakPoint->leftBound << "-"
                  << leftBreakPoint->rightBound << " & "
                  << rightBreakPoint->leftBound << "-"
                  << rightBreakPoint->rightBound;
        return strStream.str();
    }
};


#endif //INC_3SEQ_BREAKPOINT_H
