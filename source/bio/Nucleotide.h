//
// Created by lamhm on 28/02/2022.
//

#ifndef INC_3SEQ_NUCLEOTIDE_H
#define INC_3SEQ_NUCLEOTIDE_H


enum class Nucleotide : char {
    ADENINE = 'A',
    CYTOSINE = 'C',
    GUANINE = 'G',
    THYMINE = 'T',
    GAP = '-'
};

inline bool operator==(const Nucleotide &lhs, const char &rhs) {
    return static_cast<char>(lhs) == rhs;
}

inline bool operator==(const char &lhs, const Nucleotide &rhs) {
    return rhs == lhs;
}

inline bool operator!=(const Nucleotide &lhs, const char &rhs) {
    return static_cast<char>(lhs) != rhs;
}

inline bool operator!=(const char &lhs, const Nucleotide &rhs) {
    return rhs != lhs;
}


#endif //INC_3SEQ_NUCLEOTIDE_H
