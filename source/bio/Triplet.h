//
// Created by lamhm on 20/03/2022.
//

#ifndef INC_3SEQ_TRIPLET_H
#define INC_3SEQ_TRIPLET_H

#include <vector>
#include <memory>
#include <string>

#include "Sequence.h"
#include "BreakPoint.h"


class TripletPool;


class Triplet {
public:
    friend class TripletPool;

    static void setLongRecombinantThreshold(const unsigned long &newThreshold);

    static void setAcceptApproxPVal(const bool &acceptApproxPVal);

    static std::shared_ptr<Triplet> create();

    // Forbid all move and copy constructors
    Triplet(const Triplet &rhs) = delete;

    Triplet(Triplet &&) = delete;

    Triplet &operator=(const Triplet &) = delete;

    Triplet &operator=(Triplet &&) = delete;

    explicit Triplet();

    void reassign(const SequencePtr &newChild, const SequencePtr &newDad, const SequencePtr &newMum);

    double getPValue() const;

    void seekBreakPointPairs();

    bool hasExactPVal() const;

    bool hasPVal() const;

    /**
     * Produce a short string that contains the names of the sequences forming the triplet as well
     * as the number of up- and down-steps of the random walk corresponding to the triplet.
     * @return A string that describes basic infos of the triplet.
     */
    std::string describe() const;

    std::string toString(const std::string &infoSeparator = ",") const;

    bool breakPointsComputed() const;

    /**
     * Generate a postscript file that represents the triplet.
     * @param fileName  Name of the output file.
     * @param nuWidth   Width of each nucleotides space in the PS file.
     * @param imgHeight Height of the image.
     */
    void generatePSFile(const std::string &fileName,
                        const unsigned long &nuWidth = 2,
                        const unsigned long &imgHeight = 240) const;

    /**
     * Check if the p-value of this triplet is smaller than that of another triplet.
     * If the 2 p-values are equal, the triplet with the longer minimum recombinant length is
     * considered as "better".
     * @param another Another triplet.
     * @return <i>true</i> if the p-value of this triplet is lower or its minimum recombinant
     *         length is longer (in the case that the p-values are equal); <i>false</i> otherwise.
     */
    bool statisticallyBetter(const Triplet &another) const;


private:
    static unsigned long longRecombinantThreshold;
    static bool isApproxPValAccepted;

    SequencePtr child;
    SequencePtr dad;
    SequencePtr mum;

    long upStep;
    long downStep;
    long maxDescent;

    /**
     * Minimum length of the recombination segment.
     */
    unsigned long minRecLength;

    double exactPValue;
    double approxPValue;

    std::vector<long long> randomWalkHeights;

    /**
     * A vector that stores the most recent maximum height up to the current active nucleotide
     * position (from the left, including the current position).
     */
    std::vector<long long> mostRecentMaxHeights;

    std::vector<BreakPointPtr> leftBreakPoints;
    std::vector<BreakPointPtr> rightBreakPoints;
    std::vector<BreakPointPair> bpPairs;

    void updateSteps();

    void computePValues();

    void seekBreakPoints();

    BreakPointPtr buildBpFromRight(unsigned long &randomWalkPos,
                                   const bool &buildingRightBp);

    unsigned long countNonGappedSites(const unsigned long &leftOrigNuPos,
                                      const unsigned long &rightOrigNuPos) const;
};


typedef std::shared_ptr<Triplet> TripletPtr;


#endif //INC_3SEQ_TRIPLET_H
