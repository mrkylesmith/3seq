//
// Created by lamhm on 02/03/2022.
//

#include "AlignmentDescriptor.h"
#include <stdexcept>
#include <limits>


AlignmentDescriptor::AlignmentDescriptor(Alignment &alignment)
        : alignment(&alignment),
          allelicStats(nullptr), pairwiseDistanceStats(nullptr), tripletCounts(nullptr) {
    // Do nothing
}


AlignmentDescriptor::AllelicStats::AllelicStats(const Alignment &alignment)
        : nMonoallelic(0), nBiallelic(0), nTriallelic(0), nTetrallelic(0),
          nOnlyGaps(0), nNoGap(0) {
    // Make sure the alignment is locked before calculating the stats
    if (!alignment.locked) {
        throw std::logic_error(
                "Allelic statistics cannot be calculated for an unlocked alignment.");
    }

    // Scan through the allelic markers of the ACTIVE columns in the alignment
    // and update the statistics
    for (const auto &columnIdx: *(alignment.activeNuPositions)) {
        auto allelicMarker = alignment.allelicMarkers[columnIdx];
        if (allelicMarker == AllelicMask::GAP) {
            this->nOnlyGaps++;

        } else {
            if (!(allelicMarker & AllelicMask::GAP)) {
                this->nNoGap++;
            }

            if (allelicMarker & AllelicMask::MONO) {
                this->nMonoallelic++;
            } else if (allelicMarker & AllelicMask::BI) {
                this->nBiallelic++;
            } else if (allelicMarker & AllelicMask::TRI) {
                this->nTriallelic++;
            } else if (allelicMarker & AllelicMask::TETRA) {
                this->nTetrallelic++;
            }
        }
    }
}


AlignmentDescriptor::PairwiseDistanceStats::PairwiseDistanceStats(const Alignment &alignment)
        : nPwComparisons(0), minPwDist(std::numeric_limits<unsigned long>::max()), maxPwDist(0),
          meanPwDist(0.0L), meanPwDistPerSite(0.0L) {
    // Make sure the alignment is locked before calculating the stats
    if (!alignment.locked) {
        throw std::logic_error(
                "Pairwise-distance statistics cannot be calculated for an unlocked alignment.");
    }

    long double totalPwDist = 0.0L;
    for (const auto &parentSeq: alignment.getActiveParents()) {
        for (const auto &childSeq: alignment.getActiveChildren()) {
            if (parentSeq != childSeq) {
                nPwComparisons++;
                auto newPwDist = childSeq->ntDistanceTo(*parentSeq, true);
                if (newPwDist > 0) {
                    totalPwDist += static_cast<long double>(newPwDist);
                }
                if (minPwDist > newPwDist) {
                    minPwDist = newPwDist;
                }
                if (maxPwDist < newPwDist) {
                    maxPwDist = newPwDist;
                }
            }
        }
    }
    meanPwDist = totalPwDist / static_cast<long double>(nPwComparisons);
    meanPwDistPerSite = meanPwDist / static_cast<long double>(alignment.activeLength());
}


const std::unique_ptr<AlignmentDescriptor::AllelicStats> &
AlignmentDescriptor::getAllelicStats() {
    if (allelicStats == nullptr) {
        allelicStats = std::make_unique<AllelicStats>(*alignment);
    }
    return allelicStats;
}


const std::unique_ptr<AlignmentDescriptor::PairwiseDistanceStats> &
AlignmentDescriptor::getPwDistanceStats() {
    if (pairwiseDistanceStats == nullptr) {
        pairwiseDistanceStats = std::make_unique<PairwiseDistanceStats>(*alignment);
    }
    return pairwiseDistanceStats;
}


AlignmentDescriptor::TripletCounts::TripletCounts(const Alignment &alignment)
        : all(0), used(0), active(0) {

    // Make sure the alignment is locked before calculating the stats
    if (!alignment.locked) {
        throw std::logic_error("Triplet count cannot be performed on an unlocked alignment.");
    }

    auto nAllChildren = alignment.childPool.countAllSequences();
    auto nUsedChildren = alignment.childPool.countUsedSequences();
    auto nActiveChildren = alignment.childPool.countActiveSequences();

    auto nAllParents = alignment.parentPool.countAllSequences();
    auto nUsedParents = alignment.parentPool.countUsedSequences();
    auto nActiveParents = alignment.parentPool.countActiveSequences();
    if (alignment.isChildParentFromSamePool()) {
        nAllParents--;
        nUsedParents--;
        nActiveParents--;
    }

    // Calculate the number of triplets that can be formed from the alignment
    if (nAllParents >= 2 && nAllChildren >= 1) {
        this->all = nAllChildren * nAllParents * (nAllParents - 1);
    }
    if (nUsedParents >= 2 && nUsedChildren >= 1) {
        this->used = nUsedChildren * nUsedParents * (nUsedParents - 1);
    }
    if (nActiveParents >= 2 && nActiveChildren >= 1) {
        this->active = nActiveChildren * nActiveParents * (nActiveParents - 1);
    }
}


const std::unique_ptr<AlignmentDescriptor::TripletCounts> &AlignmentDescriptor::getTripletCounts() {
    if (tripletCounts == nullptr) {
        tripletCounts = std::make_unique<TripletCounts>(*alignment);
    }
    return tripletCounts;
}
