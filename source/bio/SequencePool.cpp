//
// Created by lamhm on 11/04/2022.
//

#include "SequencePool.h"
#include "../util/numeric_types.h"

#include <stdexcept>


SequencePool::SequencePool()
        : allSequences(), usedSequences(), activeSequences(), sequenceStatuses() {
    // Do nothing
}


void SequencePool::addSequence(const SequencePtr &sequencePtr) {
    if (!this->allSequences.empty()
        && sequencePtr->activeLength() != this->getSequenceActiveLength()) {
        throw std::length_error(
                "Unmatched sequence length is found in sequence '" + sequencePtr->getName()
                + "' (expected: " + std::to_string(this->getSequenceActiveLength())
                + ", received: " + std::to_string(sequencePtr->activeLength()) + ").");

    }
    if (findSequenceByName(sequencePtr->getName()) != nullptr) {
        throw std::runtime_error("Duplicate sequence name detected: "
                                 + sequencePtr->getName() + ".");

    } else {
        this->allSequences.push_back(sequencePtr);
        this->usedSequences.push_back(sequencePtr);
        this->activeSequences.push_back(sequencePtr);
        this->sequenceStatuses.push_back(SequenceStatus::ACTIVE);
    }
}


SequencePtr SequencePool::findSequenceByName(const std::string &sequenceName) const {
    for (auto sequence: allSequences) {
        if (sequence->getName() == sequenceName) {
            return sequence;
        }
    }

    // Sequence name not found
    return nullptr;
}


unsigned long SequencePool::findSequenceIdxByName(const std::string &sequenceName) const {
    auto seqCount = allSequences.size();
    for (unsigned long seqIdx = 0; seqIdx < seqCount; seqIdx++) {
        if (allSequences[seqIdx]->getName() == sequenceName) {
            return seqIdx;
        }
    }
    return ULong::NOT_SET;
}


unsigned long SequencePool::getSequenceActiveLength() const {
    if (activeSequences.empty()) {
        return 0;
    } else {
        return activeSequences.front()->activeLength();
    }
}


unsigned long SequencePool::getSequenceFullLength() const {
    if (allSequences.empty()) {
        return 0;
    } else {
        return allSequences.front()->fullLength();
    }
}


void SequencePool::cacheSequenceStatuses() {
    usedSequences.clear();
    activeSequences.clear();

    auto seqCount = allSequences.size();
    for (unsigned long seqIdx = 0; seqIdx < seqCount; seqIdx++) {
        if (sequenceStatuses[seqIdx] == SequenceStatus::ACTIVE) {
            activeSequences.push_back(allSequences[seqIdx]);
            usedSequences.push_back(allSequences[seqIdx]);
        } else if (sequenceStatuses[seqIdx] == SequenceStatus::INACTIVE) {
            usedSequences.push_back(allSequences[seqIdx]);
        }
    }
}


void SequencePool::activateSequencesBy1BasedIdxRange(const unsigned long &startIdx,
                                                     const unsigned long &endIdx) {
    if (!isSet(startIdx) || !isSet(endIdx)) {
        return;
    }

    auto seqCount = allSequences.size();
    if (1 > startIdx || startIdx > endIdx || endIdx > seqCount) {
        throw std::range_error(
                "Invalid 1-based index range: [" + std::to_string(startIdx)
                + ", " + std::to_string(endIdx) + "].");
    }

    return activateSequencesByIdxRange(startIdx - 1, endIdx - 1);
}


void SequencePool::activateSequencesByIdxRange(const unsigned long &startIdx,
                                               const unsigned long &endIdx) {
    if (!isSet(startIdx) || !isSet(endIdx)) {
        return;
    }

    auto seqCount = allSequences.size();
    if (0 > startIdx || startIdx > endIdx || endIdx >= seqCount) {
        throw std::range_error(
                "Invalid 0-based index range: [" + std::to_string(startIdx)
                + ", " + std::to_string(endIdx) + "].");
    }

    for (unsigned long idx = 0; idx < seqCount; idx++) {
        if (sequenceStatuses[idx] == SequenceStatus::UNUSED) {
            continue;
        }

        if (startIdx <= idx && idx <= endIdx) {
            sequenceStatuses[idx] = SequenceStatus::ACTIVE;
        } else {
            sequenceStatuses[idx] = SequenceStatus::INACTIVE;
        }
    }

    cacheSequenceStatuses();
}


void SequencePool::activateSequencesByNames(const std::vector<std::string> &sequenceNames) {
    auto seqCount = allSequences.size();

    // Deactivate all used sequences
    for (unsigned long seqIdx = 0; seqIdx < seqCount; seqIdx++) {
        if (sequenceStatuses[seqIdx] != SequenceStatus::UNUSED) {
            sequenceStatuses[seqIdx] = SequenceStatus::INACTIVE;
        }
    }

    // Activate the sequences whose names appear in the given list
    for (const auto &name: sequenceNames) {
        auto seqIdx = findSequenceIdxByName(name);
        if (seqIdx < seqCount) {
            if (sequenceStatuses[seqIdx] != SequenceStatus::UNUSED) {
                sequenceStatuses[seqIdx] = SequenceStatus::ACTIVE;
            }
        } else {
            throw std::runtime_error("Sequence not found: " + name + ".");
        }
    }

    cacheSequenceStatuses();
}


unsigned long SequencePool::countAllSequences() const {
    return allSequences.size();
}


unsigned long SequencePool::countUsedSequences() const {
    return usedSequences.size();
}


unsigned long SequencePool::countActiveSequences() const {
    return activeSequences.size();
}


const std::vector<SequencePtr> &SequencePool::getActiveSequences() const {
    return this->activeSequences;
}


const std::vector<SequencePtr> &SequencePool::getUsedSequences() const {
    return this->usedSequences;
}


unsigned long SequencePool::countDistinctSequences() const {
    unsigned long distinctSeqCount = 0;
    auto allSeqCount = allSequences.size();

    for (unsigned long seqIdx = 0; seqIdx < allSeqCount; seqIdx++) {
        if (sequenceStatuses[seqIdx] == SequenceStatus::UNUSED) {
            continue;
        }

        auto sequence = allSequences[seqIdx];
        bool isDistinct = true;
        for (unsigned long anotherSeqIdx = 0; anotherSeqIdx < allSeqCount; anotherSeqIdx++) {
            if (anotherSeqIdx == seqIdx
                || sequenceStatuses[anotherSeqIdx] == SequenceStatus::UNUSED) {
                continue;
            }
            if (sequence->isSubSequenceOf(*(allSequences[anotherSeqIdx]))) {
                isDistinct = false;
                break;  // inner loop
            }
        }
        if (isDistinct) {
            distinctSeqCount++;
        }
    }

    return distinctSeqCount;
}


void SequencePool::excludeNonDistinctSequences() {
    auto allSeqCount = allSequences.size();

    for (unsigned long seqIdx = 0; seqIdx < allSeqCount; seqIdx++) {
        if (sequenceStatuses[seqIdx] == SequenceStatus::UNUSED) {
            continue;
        }

        auto sequence = allSequences[seqIdx];
        for (unsigned long anotherSeqIdx = 0; anotherSeqIdx < allSeqCount; anotherSeqIdx++) {
            if (anotherSeqIdx == seqIdx
                || sequenceStatuses[anotherSeqIdx] == SequenceStatus::UNUSED) {
                continue;
            }
            if (allSequences[anotherSeqIdx]->isSubSequenceOf(*sequence)) {
                sequenceStatuses[anotherSeqIdx] = SequenceStatus::UNUSED;
            }
        }
    }

    cacheSequenceStatuses();
}


void SequencePool::excludeNeighboringSequences(const unsigned long &maxNeighborDistance) {
    if (maxNeighborDistance == 0) {
        return excludeNonDistinctSequences();
    }

    auto seqCount = allSequences.size();

    // Count the number of gaps in each sequence
    std::vector<unsigned long> gapCounts(seqCount, 0);
    for (unsigned long seqIdx = 0; seqIdx < seqCount; seqIdx++) {
        if (sequenceStatuses[seqIdx] == SequenceStatus::UNUSED) {
            continue;
        }
        gapCounts[seqIdx] = allSequences[seqIdx]->countGaps();
    }

    // Find neighboring sequences and mark those with more gap as unused.
    // I.e. keep only 1 sequence (that has the fewest number of gaps) for each neighboring group.
    for (unsigned long seqIdx = 0; seqIdx < (seqCount - 1); seqIdx++) {
        if (sequenceStatuses[seqIdx] == SequenceStatus::UNUSED) {
            continue;
        }

        auto sequence = allSequences[seqIdx];
        for (unsigned long anotherSeqIdx = (seqIdx + 1);
             anotherSeqIdx < seqCount; anotherSeqIdx++) {
            if (sequenceStatuses[anotherSeqIdx] == SequenceStatus::UNUSED) {
                continue;
            }

            auto anotherSeq = allSequences[anotherSeqIdx];
            if (sequence->ntDistanceTo(*anotherSeq) <= maxNeighborDistance) {
                if (gapCounts[seqIdx] <= gapCounts[anotherSeqIdx]) {
                    sequenceStatuses[anotherSeqIdx] == SequenceStatus::UNUSED;
                } else {
                    sequenceStatuses[seqIdx] == SequenceStatus::UNUSED;
                    break;  // break the inner loop
                }
            }
        }
    }

    cacheSequenceStatuses();
}
