//
// Created by lamhm on 02/03/2022.
//

#ifndef INC_3SEQ_ALIGNMENT_DESCRIPTOR_H
#define INC_3SEQ_ALIGNMENT_DESCRIPTOR_H

#include <memory>
#include "Alignment.h"


class AlignmentDescriptor {
public:
    AlignmentDescriptor() = delete;

    class AllelicStats;

    class PairwiseDistanceStats;

    class TripletCounts;

    explicit AlignmentDescriptor(Alignment &alignment);

    const std::unique_ptr<AllelicStats> &getAllelicStats();

    const std::unique_ptr<PairwiseDistanceStats> &getPwDistanceStats();

    const std::unique_ptr<TripletCounts> &getTripletCounts();

private:
    Alignment *const alignment;

    std::unique_ptr<AllelicStats> allelicStats;

    std::unique_ptr<PairwiseDistanceStats> pairwiseDistanceStats;

    std::unique_ptr<TripletCounts> tripletCounts;
};


class AlignmentDescriptor::AllelicStats {
public:
    /** Number of mono-allelic columns */
    unsigned long nMonoallelic;

    /** Number of bi-allelic columns */
    unsigned long nBiallelic;

    /** Number of tri-allelic columns */
    unsigned long nTriallelic;

    /** Number of tetra-allelic columns */
    unsigned long nTetrallelic;

    /** Number of columns that contain only gaps */
    unsigned long nOnlyGaps;

    /** Number of columns that have no gap */
    unsigned long nNoGap;

    explicit AllelicStats(const Alignment &alignment);
};


class AlignmentDescriptor::PairwiseDistanceStats {
public:
    unsigned long long nPwComparisons;
    unsigned long minPwDist;
    unsigned long maxPwDist;
    long double meanPwDist;
    long double meanPwDistPerSite;

    explicit PairwiseDistanceStats(const Alignment &alignment);
};


class AlignmentDescriptor::TripletCounts {
public:
    unsigned long long all;
    unsigned long long used;
    unsigned long long active;

    explicit TripletCounts(const Alignment &alignment);
};


#endif //INC_3SEQ_ALIGNMENT_DESCRIPTOR_H
