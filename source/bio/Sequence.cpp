//
// Created by lamhm on 16/02/2022.
//

#include "Sequence.h"

#include <utility>


std::string Sequence::preprocessDnaString(const std::string &dnaString) {
    std::string processedStr;

    for (char nuChar: dnaString) {
        auto capitalisedNuChar = toupper(nuChar);
        switch (capitalisedNuChar) {
            case 'U':
                capitalisedNuChar = static_cast<char>(Nucleotide::THYMINE);
                break;
            case 'A':
            case 'C':
            case 'G':
            case 'T':
            case '-':
                break;
            default:
                // Replace all ambiguous characters by gaps
                capitalisedNuChar = static_cast<char>(Nucleotide::GAP);
        }
        processedStr += static_cast<char>(capitalisedNuChar);
    }

    return processedStr;
}


std::shared_ptr<Sequence> Sequence::create(const std::string &name, const std::string &dnaString) {
    return std::make_shared<Sequence>(name, dnaString);
}


Sequence::Sequence(std::string name, const std::string &dnaString)
        : name(std::move(name)), allNucleotides(), activePositions(nullptr),
          recombinantType(RecombinantType::NOT_RECOM) {

    auto processedDnaStr = preprocessDnaString(dnaString);
    for (auto nuChar: processedDnaStr) {
        this->allNucleotides.push_back(Nucleotide(nuChar));
    }
}


std::string Sequence::toString() const {
    std::string sequenceStr;
    auto seqLen = activeLength();
    for (unsigned long activeNuIdx = 0; activeNuIdx < seqLen; activeNuIdx++) {
        sequenceStr += static_cast<char>(getActiveNu(activeNuIdx));
    }
    return sequenceStr;
}


unsigned long Sequence::activeLength() const {
    if (activePositions) {
        return activePositions->size();
    } else {
        return allNucleotides.size();
    }
}


unsigned long Sequence::fullLength() const {
    return allNucleotides.size();
}


Sequence::~Sequence() {
    this->activePositions = nullptr;  // Do not call delete on activePositions
}


const std::string &Sequence::getName() const {
    return name;
}


unsigned long Sequence::ntDistanceTo(const Sequence &another, const bool &ignoreGap) const {
    auto thisLen = this->activeLength();
    auto anotherLen = another.activeLength();

		/*
    if (thisLen != anotherLen) {
        throw std::length_error("Distance calculation requires sequences of the same length.");
    }
		*/

    unsigned long distance = 0;
    for (unsigned long activeNuIdx = 0; activeNuIdx < thisLen; activeNuIdx++) {
        auto thisNu = this->getActiveNu(activeNuIdx);
        auto anotherNu = another.getActiveNu(activeNuIdx);
        if (ignoreGap && (thisNu == Nucleotide::GAP || anotherNu == Nucleotide::GAP)) {
            continue;
        } else if (thisNu != anotherNu) {
            distance++;
        }
    }

    return distance;
}


bool Sequence::isSubSequenceOf(const Sequence &another) const {
    auto thisLen = this->activeLength();
    auto anotherLen = another.activeLength();

		/*
    if (thisLen != anotherLen) {
        throw std::length_error("Sub-sequence detection requires sequences of the same length.");
    }
		*/

    for (unsigned long activeNuIdx = 0; activeNuIdx < thisLen; activeNuIdx++) {
        auto thisNu = this->getActiveNu(activeNuIdx);
        auto anotherNu = another.getActiveNu(activeNuIdx);
        if (thisNu != Nucleotide::GAP && thisNu != anotherNu) {
            return false;
        }
    }
    return true;
}

//Sequence::UseStatus Sequence::getUseStatus() const {
//    return this->useStatus;
//}
//
//void Sequence::setUseStatus(const Sequence::UseStatus &newUseStatus) {
//    if (this->useStatus != UseStatus::UNUSED) {
//        this->useStatus = newUseStatus;
//    }
//}


unsigned long Sequence::countGaps() const {
    unsigned long gapCount = 0;

    auto seqActiveLen = this->activeLength();
    for (unsigned long activeNuIdx = 0; activeNuIdx < seqActiveLen; activeNuIdx++) {
        if (this->getActiveNu(activeNuIdx) == Nucleotide::GAP) {
            gapCount++;
        }
    }

    return gapCount;
}


unsigned long Sequence::getOrigPosOfActiveNu(const unsigned long &activeNuIdx) const {
    if (activePositions) {
        return (*activePositions)[activeNuIdx];
    } else {
        return activeNuIdx;
    }
}


const Nucleotide &Sequence::getActiveNu(const unsigned long &activeNuIdx) const {
    return allNucleotides[getOrigPosOfActiveNu(activeNuIdx)];
}


Sequence::RecombinantType Sequence::getRecombinantType() const {
    return recombinantType;
}


void Sequence::setRecombinantType(Sequence::RecombinantType newRecombinantType) {
    recombinantType = newRecombinantType;
}


