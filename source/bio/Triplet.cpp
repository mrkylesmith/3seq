//
// Created by lamhm on 20/03/2022.
//

#include "Triplet.h"

#include "../stat/PTable.h"
#include "../util/numeric_types.h"


unsigned long Triplet::longRecombinantThreshold = 100;
bool Triplet::isApproxPValAccepted = true;


void Triplet::setLongRecombinantThreshold(const unsigned long &newThreshold) {
    Triplet::longRecombinantThreshold = newThreshold;
}


void Triplet::setAcceptApproxPVal(const bool &acceptApproxPVal) {
    Triplet::isApproxPValAccepted = acceptApproxPVal;
}


std::shared_ptr<Triplet> Triplet::create() {
    return std::make_shared<Triplet>();
}


Triplet::Triplet()
        : child(nullptr), dad(nullptr), mum(nullptr),
          leftBreakPoints(), rightBreakPoints(),
          upStep(0), downStep(0), maxDescent(0), minRecLength(0),
          exactPValue(Double::NOT_SET), approxPValue(Double::NOT_SET),
          randomWalkHeights(), mostRecentMaxHeights() {
    // Do nothing
}


void Triplet::reassign(const SequencePtr &newChild,
                       const SequencePtr &newDad,
                       const SequencePtr &newMum) {
    child = newChild;
    dad = newDad;
    mum = newMum;

    minRecLength = 0;

    leftBreakPoints.clear();
    rightBreakPoints.clear();
    bpPairs.clear();

    updateSteps();
    computePValues();
}


void Triplet::updateSteps() {
    unsigned long activeSeqLen = child->activeLength();

    upStep = 0;
    downStep = 0;
    maxDescent = 0;

    randomWalkHeights.resize(activeSeqLen + 1);
    mostRecentMaxHeights.resize(activeSeqLen + 1);
    randomWalkHeights[0] = 0;
    mostRecentMaxHeights[0] = 0;

    for (unsigned long activeNuIdx = 0; activeNuIdx < activeSeqLen; activeNuIdx++) {
        auto dadNu = dad->getActiveNu(activeNuIdx);
        auto mumNu = mum->getActiveNu(activeNuIdx);
        auto childNu = child->getActiveNu(activeNuIdx);

        auto currentHeight = randomWalkHeights[activeNuIdx];
        if (dadNu != Nucleotide::GAP && mumNu != Nucleotide::GAP && childNu != Nucleotide::GAP) {
            if (dadNu == childNu && mumNu != childNu) {
                /* Up step */
                upStep++;
                currentHeight++;

            } else if (dadNu != childNu && mumNu == childNu) {
                /* Down step */
                downStep++;
                currentHeight--;
            }
        }
        randomWalkHeights[activeNuIdx + 1] = currentHeight;

        if (currentHeight > mostRecentMaxHeights[activeNuIdx]) {
            mostRecentMaxHeights[activeNuIdx + 1] = currentHeight;
        } else {
            mostRecentMaxHeights[activeNuIdx + 1] = mostRecentMaxHeights[activeNuIdx];
        }

        auto maxDescentToThisPoint = mostRecentMaxHeights[activeNuIdx + 1] - currentHeight;
        if (maxDescent < maxDescentToThisPoint) {
            maxDescent = maxDescentToThisPoint;
        }
    }
}


void Triplet::computePValues() {
    exactPValue = Double::NOT_SET;
    approxPValue = Double::NOT_SET;

    if (PTable::instance().canCalculateExact(upStep, downStep, maxDescent)) {
        exactPValue = PTable::instance().getExactPValue(upStep, downStep, maxDescent);
    } else if (isApproxPValAccepted) {
        approxPValue = PTable::approxPValue(upStep,
                                            downStep,
                                            maxDescent);
    }
}

double Triplet::getPValue() const {
    if (hasExactPVal()) {
        return exactPValue;
    } else {
        return approxPValue;
    }
}


void Triplet::seekBreakPoints() {
    if (!leftBreakPoints.empty() && !rightBreakPoints.empty()) {
        return; // break-points are already calculated
    }

    auto randomWalkLength = randomWalkHeights.size();
    auto randomWalkPos = randomWalkLength; // walk back from right to left
    long heightOfLeftBp = Long::NOT_SET;
    do {
        randomWalkPos--;
        if (mostRecentMaxHeights[randomWalkPos] == randomWalkHeights[randomWalkPos] + maxDescent) {
            // This position is a right breakpoint
            // WARNING: randomWalkPos will be modified by the function buildBpFromRight()
            auto breakPoint = buildBpFromRight(randomWalkPos, true);
            rightBreakPoints.push_back(breakPoint);
            heightOfLeftBp = mostRecentMaxHeights[randomWalkPos];

        } else if (randomWalkHeights[randomWalkPos] == heightOfLeftBp
                   && !rightBreakPoints.empty()) {
            // This position is a left breakpoint
            // WARNING: randomWalkPos will be modified by the function buildBpFromRight()
            auto breakPoint = buildBpFromRight(randomWalkPos, false);
            leftBreakPoints.push_back(breakPoint);
        }
    } while (randomWalkPos > 0);
}


BreakPointPtr Triplet::buildBpFromRight(unsigned long &randomWalkPos,
                                        const bool &buildingRightBp) {
    auto rightActiveNuIdx = randomWalkPos;
    while (randomWalkPos > 0
           && randomWalkHeights[randomWalkPos - 1] == randomWalkHeights[randomWalkPos]) {
        randomWalkPos--;
    }
    auto leftActiveNuIdx = randomWalkPos;

    if (buildingRightBp) {
        // The active position belongs to the dad (P) sequence
        // -> left-shift it to make the position point to the end of the mum (Q) segment
        leftActiveNuIdx--;
        rightActiveNuIdx--;
    }

    unsigned long leftOrigNuPos = 0;
    if (leftActiveNuIdx > 0) {
        leftOrigNuPos = child->getOrigPosOfActiveNu(leftActiveNuIdx - 1) + 1;
    }
    unsigned long rightOrigNuPos = child->fullLength() - 1;
    if (rightActiveNuIdx < child->activeLength() - 1) {
        rightOrigNuPos = child->getOrigPosOfActiveNu(rightActiveNuIdx + 1) - 1;
    }

    if (buildingRightBp) {
        // The right break point need to be right-shift by 1 position
        // to cover the last nucleotide of the mum (Q) segment.
        leftOrigNuPos++;
        rightOrigNuPos++;
    }

    return BreakPoint::create(leftOrigNuPos,
                              rightOrigNuPos,
                              randomWalkHeights[randomWalkPos]);
}


void Triplet::seekBreakPointPairs() {
    if (!bpPairs.empty()) {
        return; // break-point pairs are already calculated
    }

    seekBreakPoints();
    minRecLength = ULong::MAX;

    // Loop through the breakpoint vectors from right to left since the leftmost breakpoints
    // are stored at the right-end of the vectors.
    for (auto leftBpIter = leftBreakPoints.rbegin();
         leftBpIter != leftBreakPoints.rend(); ++leftBpIter) {
        for (auto rightBpIter = rightBreakPoints.rbegin();
             rightBpIter != rightBreakPoints.rend(); ++rightBpIter) {

            if ((*leftBpIter)->rightBound < (*rightBpIter)->leftBound) {
                if ((*leftBpIter)->height - (*rightBpIter)->height == maxDescent) {
                    bpPairs.emplace_back(*leftBpIter, *rightBpIter);

                    auto mumSegmentLength = countNonGappedSites(
                            (*leftBpIter)->rightBound,
                            (*rightBpIter)->leftBound);
                    if (minRecLength > mumSegmentLength) {
                        minRecLength = mumSegmentLength;
                    }

                    auto dadSegmentLength =
                            countNonGappedSites(0,
                                                (*leftBpIter)->leftBound)
                            + countNonGappedSites((*rightBpIter)->rightBound,
                                                  child->fullLength());
                    if (minRecLength > dadSegmentLength) {
                        minRecLength = dadSegmentLength;
                    }

                } else {
                    break; // break the inner loop
                }
            }
        }
    }

    if (minRecLength >= Triplet::longRecombinantThreshold
        && child->getRecombinantType() == Sequence::RecombinantType::RECOMBINANT) {
        child->setRecombinantType(Sequence::RecombinantType::LONG_RECOM);
    }
}


unsigned long Triplet::countNonGappedSites(const unsigned long &leftOrigNuPos,
                                           const unsigned long &rightOrigNuPos) const {
    unsigned long nNonGappedSites = 0;
    for (auto origNuPos = leftOrigNuPos; origNuPos < rightOrigNuPos; origNuPos++) {
        if (child->allNucleotides[origNuPos] != Nucleotide::GAP
            && dad->allNucleotides[origNuPos] != Nucleotide::GAP
            && mum->allNucleotides[origNuPos] != Nucleotide::GAP) {
            nNonGappedSites++;
        }
    }
    return nNonGappedSites;
}


std::string Triplet::describe() const {
    std::stringstream sStream;
    sStream << dad->getName() << "\t"
            << mum->getName() << "\t"
            << child->getName() << "\t"
            << upStep << "\t"
            << downStep << "\t"
            << maxDescent;
    return sStream.str();
}

bool Triplet::hasExactPVal() const {
    return (0 <= exactPValue && exactPValue <= 1.0);
}


bool Triplet::hasPVal() const {
    return (isSet(exactPValue) || isSet(approxPValue));
}


bool Triplet::breakPointsComputed() const {
    return !bpPairs.empty();
}


void Triplet::generatePSFile(const std::string &fileName,
                             const unsigned long &nuWidth,
                             const unsigned long &imgHeight) const {
    TextFile psFile(fileName);

    if (psFile.exists()) {
        Interface::instance()
                << "The file \"" << psFile.getPath() << "\" already exists.\n"
                << "Do you want to overwrite it?";
        if (!Interface::instance().showWarning(false)) {
            /* If the user say "No" */
            return;

        } else {
            psFile.removeFile();
        }
    }

    psFile.openToWrite();

    char line[200];
    psFile.writeLine("%%!PS-Adobe-3.0 EPSF-3.0");
    sprintf(line,
            "%%%%BoundingBox: 0 0 %lu %lu",
            (child->fullLength() + 1) * nuWidth, imgHeight);
    psFile.writeLine(line);
    psFile.writeLine("%%%%EndComments\n");

    psFile.writeLine("%%%%%%%%%%%%");
    psFile.writeLine("/Times-Roman findfont");
    psFile.writeLine("7 scalefont");
    psFile.writeLine("setfont\n");

    psFile.writeLine("0.001 setlinewidth");
    psFile.writeLine();

    sprintf(line,
            "/w {%lu} def    %% This is the width of a small box on the plot\n", nuWidth);
    psFile.writeLine(line);

    sprintf(line,
            "/h {%lu} def    %% This is the height of a small box on the plot\n", imgHeight);
    psFile.writeLine(line);

    auto nActiveNus = child->activeLength();
    for (unsigned long activeNuIdx = 0; activeNuIdx < nActiveNus; activeNuIdx++) {

        auto dadNu = dad->getActiveNu(activeNuIdx);
        auto mumNu = mum->getActiveNu(activeNuIdx);
        auto childNu = child->getActiveNu(activeNuIdx);

        auto origNuPos = child->getOrigPosOfActiveNu(activeNuIdx);

        if (dadNu != Nucleotide::GAP && mumNu != Nucleotide::GAP && childNu != Nucleotide::GAP) {
            if (dadNu == childNu && mumNu != childNu) {
                sprintf(line, "1 0 0 setrgbcolor %lu 3 moveto", origNuPos * nuWidth);
                psFile.writeLine(line);
                psFile.writeLine("0 h rlineto w 0 rlineto 0 h neg rlineto closepath fill stroke\n");

            } else if (dadNu != childNu && mumNu == childNu) {
                sprintf(line, "0 0 1 setrgbcolor %lu 3 moveto", origNuPos * nuWidth);
                psFile.writeLine(line);
                psFile.writeLine("0 h rlineto w 0 rlineto 0 h neg rlineto closepath fill stroke\n");

            } else if (dadNu != childNu || mumNu != childNu) {
                // Draw a gray line if there is polymorphism but it is not an informative site
                sprintf(line, "0.4 0.4 0.4 setrgbcolor %lu 3 moveto", origNuPos * nuWidth);
                psFile.writeLine(line);

                sprintf(line, "0 %lu rlineto w 0 rlineto 0 h neg rlineto closepath fill stroke\n",
                        imgHeight / 3);
                psFile.writeLine(line);
            }
        }
    }

    psFile.writeLine("showpage");
    psFile.writeLine("%%EOF");

    psFile.close();
}


bool Triplet::statisticallyBetter(const Triplet &another) const {
    if (this->hasPVal() && !another.hasPVal()) {
        return true;
    } else if (this->hasPVal() && another.hasPVal()) {
        if ((this->getPValue() < another.getPValue())
            || (this->getPValue() == another.getPValue()
                && this->minRecLength > another.minRecLength)) {
            return true;
        }
    }
    return false;
}


std::string Triplet::toString(const std::string &infoSeparator) const {
    std::string tripletInfo;
    char stringBuffer[10000];

    auto pValue = getPValue();
    auto dunnSidak = stat::correction::dunnSidak(pValue);

    auto separatorCStr = infoSeparator.c_str();
    sprintf(stringBuffer,
            "%s%s%s%s%s%s%lu%s%lu%s%lu%s%1.12f%s%d%s%2.4f%s%1.5Lf%s%Le",
            dad->getName().c_str(), separatorCStr,
            mum->getName().c_str(), separatorCStr,
            child->getName().c_str(), separatorCStr,
            upStep, separatorCStr,
            downStep, separatorCStr,
            maxDescent, separatorCStr,
            pValue, separatorCStr,
            (!hasExactPVal()), separatorCStr,
            log10(pValue), separatorCStr,
            dunnSidak, separatorCStr,
            dunnSidak
    );
    tripletInfo = std::string(stringBuffer);

    if (breakPointsComputed()) {
        sprintf(stringBuffer, "%s%lu", separatorCStr, minRecLength);
        tripletInfo += std::string(stringBuffer);
        for (const auto &bpPair: bpPairs) {
            tripletInfo += infoSeparator + bpPair.toString();
        }
    }

    return tripletInfo;
}


