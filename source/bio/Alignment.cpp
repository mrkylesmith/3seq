//
// Created by lamhm on 16/02/2022.
//

#include "Alignment.h"
#include "../util/numeric_types.h"

#include <stdexcept>
//#include <algorithm>


Alignment::Alignment()
        : allelicMarkers(), parentPool(), childPool(),
          fullSequenceLength(0), locked(false), isSinglePool(true) {
    activeNuPositions = std::make_shared<std::vector<unsigned long>>();
}


void Alignment::addSequence(const SequencePtr &sequencePtr,
                            const bool &asParent,
                            const bool &asChild) {
    if (this->locked) {
        throw std::logic_error(
                "Sequence adding is not allowed after the alignment has been locked.");
    }
    if (!asParent && !asChild) {
        throw std::logic_error(
                "Adding sequences to the alignment -- invalid options specified.");
    }

    auto seqLen = sequencePtr->fullLength();


    if (this->fullSequenceLength == 0) {
        this->fullSequenceLength = sequencePtr->fullLength();
    } else if (this->fullSequenceLength != seqLen) {
        throw std::length_error(
                "Unmatched sequence length is found in sequence '" + sequencePtr->getName()
                + "' (expected: " + std::to_string(this->fullSequenceLength)
                + ", received: " + std::to_string(seqLen) + ").");
    }

    sequencePtr->activePositions = this->activeNuPositions;
    if (asParent) {
        parentPool.addSequence(sequencePtr);
    }
    if (asChild) {
        childPool.addSequence(sequencePtr);
    }

    if (asParent != asChild) {
        this->isSinglePool = false;
    }
}


void Alignment::addSequences(const std::vector<SequencePtr> &sequenceList,
                             const bool &asParent,
                             const bool &asChild) {
    for (const auto &sequence: sequenceList) {
        this->addSequence(sequence, asParent, asChild);
    }
}


unsigned long Alignment::activeLength() const {
    return activeNuPositions->size();
}


unsigned long Alignment::fullLength() const {
    return fullSequenceLength;
}


bool Alignment::isChildParentFromSamePool() const {
    return this->isSinglePool;
}


void Alignment::lock() {
    if (!this->locked) {
        this->populatePositionVector();
        this->markAllelicStatuses();

        this->locked = true;
    }
}


void Alignment::populatePositionVector() {
    auto alignmentLen = this->fullLength();

    activeNuPositions->reserve(alignmentLen);
    for (unsigned long idx = 0; idx < alignmentLen; idx++) {
        activeNuPositions->push_back(idx);
    }
}


void Alignment::markAllelicStatuses() {
    auto allUsedSequences = getAllUsedSequences();
    auto nUsedSequences = allUsedSequences.size();
    if (nUsedSequences <= 0) {
        throw std::logic_error("The alignment does not contain any sequences; analysis halted.");
    }

    this->allelicMarkers.clear();
    this->allelicMarkers.reserve(fullSequenceLength);

    for (unsigned long nuPos = 0; nuPos < fullSequenceLength; nuPos++) {
        auto allelicMarker = AllelicMask::EMPTY;
        for (const auto &sequence: allUsedSequences) {
            switch (sequence->allNucleotides[nuPos]) {
                case Nucleotide::GAP:
                    allelicMarker |= AllelicMask::GAP;
                    break;
                case Nucleotide::ADENINE:
                    allelicMarker |= AllelicMask::ADENINE;
                    break;
                case Nucleotide::CYTOSINE:
                    allelicMarker |= AllelicMask::CYTOSINE;
                    break;
                case Nucleotide::GUANINE:
                    allelicMarker |= AllelicMask::GUANINE;
                    break;
                case Nucleotide::THYMINE:
                    allelicMarker |= AllelicMask::THYMINE;
                    break;
            }
            if (allelicMarker & AllelicMask::TETRA_WITH_GAP) {
                // All possible nucleotide characters have been found in the column.
                // No need to check more sequences -> stop the inner loop.
                break;
            }
        }

        // Detect the polymorphic state of the column.
        // This is not necessary for tetrallelic sites since it would have already been marked after
        // the for loop above.
        if (!(allelicMarker & AllelicMask::TETRA)) {
            int nAlleles = 0;
            if (allelicMarker & AllelicMask::ADENINE) {
                nAlleles++;
            }
            if (allelicMarker & AllelicMask::CYTOSINE) {
                nAlleles++;
            }
            if (allelicMarker & AllelicMask::GUANINE) {
                nAlleles++;
            }
            if (allelicMarker & AllelicMask::THYMINE) {
                nAlleles++;
            }

            switch (nAlleles) {
                case 1:
                    allelicMarker |= AllelicMask::MONO;
                    break;
                case 2:
                    allelicMarker |= AllelicMask::BI;
                    break;
                case 3:
                    allelicMarker |= AllelicMask::TRI;
                    break;
                default:
                    // Do nothing
                    break;
            }
        }

        // Save the allelic marker
        this->allelicMarkers.push_back(allelicMarker);
    }
}


void Alignment::shrink(unsigned long beginPos, unsigned long endPos, const bool &isOneBasedIdx) {
    if (!isSet(beginPos) || !isSet(endPos)) {
        return;
    }

    // If beginPos and endPos are 1-based indices, convert them back to 0-based indices
    if (isOneBasedIdx) {
        beginPos--;
        endPos--;
    }

    // Make sure the alignment has been locked before processing it
    this->lock();

    // Validate beginPos and endPos
    auto minNuPos = (*activeNuPositions).front();
    auto maxNuPos = (*activeNuPositions).back();
    if (minNuPos > beginPos || beginPos > endPos || endPos > maxNuPos) {
        throw std::invalid_argument(
                "Alignment shrinking cannot be performed due to invalid cutting positions.");
    }

    // Shift (by copying) columns between beginPos and endPos to the left
    unsigned long newActiveLen = 0;  // to store the active length of the alignment after shrinking
    for (auto nuPos: (*activeNuPositions)) {
        if (beginPos <= nuPos && nuPos <= endPos) {
            (*activeNuPositions)[newActiveLen] = nuPos;
            newActiveLen++;
        } else if (nuPos > endPos) {
            break;
        }
    }
    activeNuPositions->resize(newActiveLen);
}


void Alignment::cutOut(unsigned long beginPos, unsigned long endPos, const bool &isOneBasedIdx) {
    if (!isSet(beginPos) || !isSet(endPos)) {
        return;
    }

    // If beginPos and endPos are 1-based indices, convert them back to 0-based indices
    if (isOneBasedIdx) {
        beginPos--;
        endPos--;
    }

    // Make sure the alignment has been locked before processing it
    this->lock();

    // Validate beginPos and endPos
    auto minNuPos = (*activeNuPositions).front();
    auto maxNuPos = (*activeNuPositions).back();
    if (minNuPos > beginPos || beginPos > endPos || endPos > maxNuPos) {
        throw std::invalid_argument(
                "Alignment cut-out cannot be performed due to invalid cutting positions.");
    }

    // Shift (by copying) columns between beginPos and endPos to the left
    unsigned long newActiveLen = 0;  // to store the active length of the alignment after shrinking
    for (auto nuPos: (*activeNuPositions)) {
        if (nuPos < beginPos || endPos < nuPos) {
            (*activeNuPositions)[newActiveLen] = nuPos;
            newActiveLen++;
        }
    }
    activeNuPositions->resize(newActiveLen);
}


void Alignment::excludeCodonPositions(const bool &first, const bool &second, const bool &third,
                                      const bool &useOrigColumnIdxs) {
    // Make sure the alignment has been locked before processing it
    this->lock();

    // Delete the specified codon positions
    auto oldActiveLen = this->activeLength();
    unsigned long newActiveLen = 0;
    for (unsigned long activeIdx = 0; activeIdx < oldActiveLen; activeIdx++) {
        auto nuPos = activeIdx;
        if (useOrigColumnIdxs) {
            nuPos = (*activeNuPositions)[activeIdx];
        }
        auto codonPos = nuPos % 3;
        if ((codonPos == 0 && !first) || (codonPos == 1 && !second) || (codonPos == 2 && !third)) {
            (*activeNuPositions)[newActiveLen] = (*activeNuPositions)[activeIdx];
            newActiveLen++;
        }
    }
    activeNuPositions->resize(newActiveLen);
}


void Alignment::excludeMonomorphicColumns() {
    // Make sure the alignment has been locked before processing it
    this->lock();

    // Delete non-polymorphic columns
    unsigned long newActiveLen = 0;
    for (const auto &activeColIdx: (*activeNuPositions)) {
        auto allelicMarker = this->allelicMarkers[activeColIdx];
        if ((allelicMarker & AllelicMask::BI) || (allelicMarker & AllelicMask::TRI)
            || (allelicMarker & AllelicMask::TETRA)) {
            (*activeNuPositions)[newActiveLen] = activeColIdx;
            newActiveLen++;
        }
    }
    activeNuPositions->resize(newActiveLen);

    if (newActiveLen <= 0) {
        throw std::logic_error(
                "The alignment does not contain any polymorphic columns; analysis halted.");
    }
}


unsigned long Alignment::nCoveredNuPositions() const {
    // Make sure the alignment has been locked
    if (!locked) {
        throw std::logic_error("The alignment must be locked.");
    }
    return activeNuPositions->back() - activeNuPositions->front() + 1;
}


const std::vector<SequencePtr> &Alignment::getActiveParents() const {
    return parentPool.getActiveSequences();
}


const std::vector<SequencePtr> &Alignment::getActiveChildren() const {
    return childPool.getActiveSequences();
}


SequencePool &Alignment::getParentPool() {
    return this->parentPool;
}


SequencePool &Alignment::getChildPool() {
    return this->childPool;
}


std::set<SequencePtr> Alignment::getAllUsedSequences() const {
    std::set<SequencePtr> allUsedSequences;
    for (auto sequence : parentPool.getUsedSequences()) {
        allUsedSequences.insert(sequence);
    }

    if (!isSinglePool) {
        for (auto sequence : childPool.getUsedSequences()) {
            allUsedSequences.insert(sequence);
        }
    }

    return allUsedSequences;
}


unsigned long Alignment::countAllSequences() const {
    auto seqCount = parentPool.countAllSequences();
    if (!isSinglePool) {
        seqCount += childPool.countAllSequences();
    }

    return seqCount;
}


unsigned long Alignment::countUsedSequences() const {
    auto seqCount = parentPool.countUsedSequences();
    if (!isSinglePool) {
        seqCount += childPool.countUsedSequences();
    }

    return seqCount;
}


unsigned long Alignment::countDistinctSequences() const {
    auto distinctSeqCount = parentPool.countDistinctSequences();
    if (!isSinglePool) {
        distinctSeqCount += childPool.countDistinctSequences();
    }

    return distinctSeqCount;
}

