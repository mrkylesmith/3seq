//
// Created by lamhm on 04/05/22.
//

#ifndef INC_3SEQ_TRIPLETPOOL_H
#define INC_3SEQ_TRIPLETPOOL_H

#include <map>
#include <vector>
#include <stack>

#include "Triplet.h"
#include "Sequence.h"
#include "../file/TextFile.h"


class TripletPool {
public:
    enum class StorageMode {
        BEST_TRIPLET,
        ALL_TRIPLETS
    };

    TripletPool();

    TripletPtr newTriplet(const SequencePtr &child,
                          const SequencePtr &dad,
                          const SequencePtr &mum);

    void saveTriplet(const SequencePtr &child, const TripletPtr& newTriplet);

    void freeTriplet(TripletPtr & triplet);

    void setStorageMode(StorageMode newStorageMode);

    void writeToFile(TextFile* fileRecombinants, const std::string &separator = ",") const;

    void seekBreakPointPairs(const SequencePtr &child);

    unsigned long getLongestMinRecLength() const;

private:
    std::map<SequencePtr, std::vector<TripletPtr>> savedTriplets;
    std::stack<TripletPtr> freeTriplets;

    StorageMode storageMode;

    unsigned long longestMinRecLength;
};


#endif //INC_3SEQ_TRIPLETPOOL_H
