//
// Created by lamhm on 11/04/2022.
//

#ifndef INC_3SEQ_SEQUENCEPOOL_H
#define INC_3SEQ_SEQUENCEPOOL_H

#include <vector>
#include <memory>
#include "Sequence.h"


class Alignment;


class SequencePool {
public:

    enum class SequenceStatus {
        /**
         * Sequence is excluded completely from the analysis.
         */
        UNUSED,

        /**
         * Sequence is used for the analysis but is temporarily deactivated
         * for the current run iteration.
         */
        INACTIVE,

        /**
         * Sequence is actively used for the current iteration of the analysis.
         */
        ACTIVE
    };

    friend class Alignment;

    // Forbid all move and copy constructors
    SequencePool(const SequencePool &rhs) = delete;

    SequencePool &operator=(const SequencePool &) = delete;

    SequencePool(SequencePool &&) = delete;

    SequencePool &operator=(SequencePool &&) = delete;

    // Allowed constructor
    explicit SequencePool();

    void addSequence(const SequencePtr &sequencePtr);

    SequencePtr findSequenceByName(const std::string &sequenceName) const;

    unsigned long findSequenceIdxByName(const std::string &sequenceName) const;

    unsigned long getSequenceActiveLength() const;

    unsigned long getSequenceFullLength() const;

    const std::vector<SequencePtr> &getActiveSequences() const;

    const std::vector<SequencePtr> &getUsedSequences() const;

    /**
     * Mark sequences whose 1-based indices are between the <b>startIdx</b> and
     * the <b>endIdx</b> inclusively as active. Used sequences whose indices are outside this
     * range will be marked as inactive.
     * @param startIdx  The 1-based index of the first child sequence to be activated.
     * @param endIdx    The 1-based index of the last child sequence to be activated.
     * @note Sequences whose use status is already set as `SequenceStatus::UNUSED` will not be
     *       affected by this method.
     */
    void activateSequencesBy1BasedIdxRange(const unsigned long &startIdx,
                                           const unsigned long &endIdx);

    /**
     * Mark sequences whose 0-based indices are between the <b>startIdx</b> and
     * the <b>endIdx</b> inclusively as active. Used sequences whose indices are outside this
     * range will be marked as inactive.
     * @param startIdx  The 0-based index of the first child sequence to be activated.
     * @param endIdx    The 0-based index of the last child sequence to be activated.
     * @note Sequences whose use status is already set as `SequenceStatus::UNUSED` will not be
     *       affected by this method.
     */
    void activateSequencesByIdxRange(const unsigned long &startIdx,
                                     const unsigned long &endIdx);

    /**
     * Set the active status of sequences whose names are in the given list. The active status of
     * the other sequences will be set at the opposite value.
     * @param sequenceNames The names of the sequences whose active statuses are to be set.
     * @param useStatus The use status to be set for the sequences in the given range. This status
     *                  must be either `UseStatus::ACTIVE` or `UseStatus::INACTIVE`.
     * @note Sequences whose use status is already set as `UseStatus::UNUSED` will not be affected
     *       by this method.
     */
    void activateSequencesByNames(const std::vector<std::string> &sequenceNames);


    unsigned long countAllSequences() const;

    unsigned long countUsedSequences() const;

    unsigned long countActiveSequences() const;

    unsigned long countDistinctSequences() const;

    /**
     * Mark non-distinct sequences as unused.
     * When 2 sequences of 0 nucleotide distance are found, the one with fewer gaps will be
     * preserved.
     */
    void excludeNonDistinctSequences();

    /**
     * Mark neighboring sequences as unused. For each group of neighboring sequences,
     * only 1 sequence that has the fewest number of gaps will be kept.
     * @param maxNeighborDistance The maximum number of nucleotide differences that a pair of
     *                            neighboring sequences can have.
     */
    void excludeNeighboringSequences(const unsigned long &maxNeighborDistance);


private:
    std::vector<SequencePtr> allSequences;
    std::vector<SequencePtr> usedSequences;
    std::vector<SequencePtr> activeSequences;
    std::vector<SequenceStatus> sequenceStatuses;

    /**
     * Loop through all sequences in the pool and store the pointers of the sequences that are
     * in use and active into the `usedSequences` and the `activeSequences` vectors respectively.
     */
    void cacheSequenceStatuses();
};


#endif //INC_3SEQ_SEQUENCEPOOL_H
