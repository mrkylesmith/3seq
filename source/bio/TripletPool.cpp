//
// Created by lamhm on 04/05/22.
//

#include "TripletPool.h"
#include <iostream>


TripletPool::TripletPool()
        : savedTriplets(), freeTriplets(), storageMode(StorageMode::BEST_TRIPLET),
          longestMinRecLength(0) {
    // Do nothing
}


void TripletPool::setStorageMode(TripletPool::StorageMode newStorageMode) {
    storageMode = newStorageMode;
}


TripletPtr TripletPool::newTriplet(const SequencePtr &child,
                                   const SequencePtr &dad,
                                   const SequencePtr &mum) {
    TripletPtr triplet;
    if (!freeTriplets.empty()) {
        triplet = freeTriplets.top();
        freeTriplets.pop();
    } else {
        triplet = Triplet::create();
    }

    triplet->reassign(child, dad, mum);
    return triplet;
}


void TripletPool::saveTriplet(const SequencePtr &child, const TripletPtr &newTriplet) {
    if (savedTriplets.find(child) != savedTriplets.end()) {
        if (storageMode == StorageMode::ALL_TRIPLETS) {
            savedTriplets[child].push_back(newTriplet);

        } else {
            // StorageMode::BEST_TRIPLET
            auto currentBestTriplet = savedTriplets[child][0];
            auto worseTriplet = newTriplet;
            if (newTriplet->statisticallyBetter(*currentBestTriplet)) {
                savedTriplets[child][0] = newTriplet;
                worseTriplet = currentBestTriplet;
            }
            freeTriplet(worseTriplet);
        }

    } else {
        savedTriplets[child] = std::vector<TripletPtr>{newTriplet};
    }
}


void TripletPool::freeTriplet(TripletPtr &triplet) {
    freeTriplets.push(triplet);
    triplet.reset();
}


void TripletPool::writeToFile(TextFile *fileRecombinants, const std::string &separator) const {
    if (fileRecombinants == nullptr) {
        return;
    }

    /* Write the header */
    fileRecombinants->writeLine(
            "P_name" + separator + "Q_name" + separator + "C_name" + separator +
            "m" + separator + "n" + separator + "k" + separator +
            "p" + separator + "HS?" + separator + "log10(p)" + separator +
            "DS(p)" + separator + "DS(p)" + separator +
            "min_rec_length" + separator + "breakpoints"
    );


    for (const auto& dataPair: savedTriplets) {
        for (const auto& triplet: dataPair.second) {
            fileRecombinants->writeLine(triplet->toString(separator));
        }
    }

    fileRecombinants->close();
}


void TripletPool::seekBreakPointPairs(const SequencePtr &child) {
    if (savedTriplets.find(child) == savedTriplets.end()) {
        return; // child not found
    }

    for (const auto& triplet : savedTriplets[child]) {
        triplet->seekBreakPointPairs();
        if (longestMinRecLength < triplet->minRecLength) {
            longestMinRecLength = triplet->minRecLength;
        }
    }
}


unsigned long TripletPool::getLongestMinRecLength() const {
    return longestMinRecLength;
}
