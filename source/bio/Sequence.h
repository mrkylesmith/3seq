//
// Created by lamhm on 16/02/2022.
//

#ifndef INC_3SEQ_SEQUENCE_H
#define INC_3SEQ_SEQUENCE_H

#include <vector>
#include <string>
#include <memory>
#include "Nucleotide.h"


class Alignment; // forward declaration to get rid of recursive include error.
class Triplet;


class Sequence {
public:
    enum class RecombinantType {
        NOT_RECOM   /** Not a recombinant */,
        RECOMBINANT /** Short recombinant */,
        LONG_RECOM  /** Long recombinant  */
    };

    // Allow Alignment and Triplet to access Sequence attributes
    friend class Alignment;
    friend class Triplet;

    /**
     * Create a new Sequence and return its shared_ptr.
     * @param name      The name of the sequence.
     * @param dnaString A string of nucleotides.
     * @return A shared_ptr to the newly created sequence.
     */
    static std::shared_ptr<Sequence> create(const std::string &name, const std::string &dnaString);

    // Forbid all move and copy constructors
    Sequence(const Sequence &rhs) = delete;

    Sequence(Sequence &&) = delete;

    Sequence &operator=(const Sequence &) = delete;

    Sequence &operator=(Sequence &&) = delete;

    /**
     * Create the sequence with the given name and DNA string.
     * @param name      The name of the sequence.
     * @param dnaString A string of nucleotides.
     * @note This constructor should not be called directly. Instead, use the Sequence::create()
     *       method to create new Sequence objects (and their shared_ptr's).
     */
    explicit Sequence(std::string name, const std::string &dnaString);

    virtual ~Sequence();

    std::string toString() const;

    /**
     * Get the number of active nucleotide sites of this sequence.
     * @return  The number of active nucleotides of this sequence.
     */
    unsigned long activeLength() const;

    unsigned long fullLength() const;

    unsigned long countGaps() const;

    const std::string &getName() const;

    unsigned long getOrigPosOfActiveNu(const unsigned long &activeNuIdx) const;

    const Nucleotide &getActiveNu(const unsigned long &activeNuIdx) const;

//    UseStatus getUseStatus() const;

    /**
     * Set the use status of the sequence.
     * @param newUseStatus The new use status of the sequence.
     * @note  This method does not affect sequences whose statuses are already set as
     *        `UseStatus::UNUSED`.
     */
//    void setUseStatus(const UseStatus &newUseStatus);


    /**
     * Count the nucleotide distance between this sequence and the given sequence.
     * @param another A sequence to compare to.
     * @param ignoreGap Indicates if gaps should be ignored in the comparison. Default: true.
     * @return The nucleotide distance
     */
    unsigned long ntDistanceTo(const Sequence &another, bool const &ignoreGap = true) const;

    /**
     * Check whether this sequence is a "sub-sequence" of another sequence.
     * Sequence A is a "sub-sequence" of sequence B if for every nucleotide position:
     *    [1] the nucleotides of both A and B are identical;
     * or [2] the nucleotide of A is a gap.
     * @param another Another sequence
     * @return  <i>true</i> if this sequence is a "sub-sequence" of <i>another</i>;
     *          <i>false</i>, otherwise.
     */
    bool isSubSequenceOf(const Sequence &another) const;

    RecombinantType getRecombinantType() const;

    void setRecombinantType(RecombinantType newRecombinantType);



private:

    /** Sequence name */
    const std::string name;

    /** The container of all the nucleotides in this sequence. */
    std::vector<Nucleotide> allNucleotides;

    /**
     * A pointer to the vector that contains active nucleotide-positions.
     * The vector is managed by the Alignment class.
     */
    std::shared_ptr<std::vector<unsigned long>> activePositions;

    /**
     * The recombinant type of this sequence. All newly constructed sequences are marked as
     * not-a-recombinant (NOT_RECOM).
     */
    RecombinantType recombinantType;

    /**
     * Pre-process a DNA string (before loading it into the nucleotides vector of this class).
     * The preprocessing includes: [1] capitalise all characters, [2] replace U (uracil) by
     * THYMINE (thymine), and [3] replace all other characters that are not 'ADENINE', 'CYTOSINE', 'G', 'THYMINE', nor
     * '-' by '-' (gap).
     * @param dnaString
     * @return
     */
    static std::string preprocessDnaString(const std::string &dnaString);

};


typedef std::shared_ptr<Sequence> SequencePtr;


#endif //INC_3SEQ_SEQUENCE_H
