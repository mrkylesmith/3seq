/* 
 * File:   SequenceFile.h
 * Author: Ha Minh Lam
 *
 * Created on 03 August 2011, 15:33
 */

#ifndef SEQUENCEFILE_H
#define    SEQUENCEFILE_H

#include <string>
#include <vector>
#include <iostream>
#include "TextFile.h"
#include "../bio/Alignment.h"
#include "../bio/Sequence.h"


////////////////////////////////////////////////////////////////////////////////
//  FASTA FILE
////////////////////////////////////////////////////////////////////////////////

class FastaFile : public TextFile {
public:
    explicit FastaFile(const std::string &newFilePath);

    virtual std::vector<SequencePtr> read();

    virtual void write(const Alignment &alignment);

    static void write(const Alignment &alignment, std::ostream *streamPtr);
};


////////////////////////////////////////////////////////////////////////////////
//  PHYLIP FILE
////////////////////////////////////////////////////////////////////////////////

class PhylipFile : public TextFile {
public:
    explicit PhylipFile(const std::string &newFilePath);

    virtual std::vector<SequencePtr> read();

    virtual void write(const Alignment &alignment);

    static void write(const Alignment &alignment, std::ostream *streamPtr);
};


////////////////////////////////////////////////////////////////////////////////
//  NEXUS FILE
////////////////////////////////////////////////////////////////////////////////

class NexusFile : public TextFile {
public:
    explicit NexusFile(const std::string &newFilePath);

    virtual std::vector<SequencePtr> read();

    virtual void write(const Alignment &alignment);

    static void write(const Alignment &alignment, std::ostream *streamPtr);
};


////////////////////////////////////////////////////////////////////////////////
//  SEQUENCE FILE
////////////////////////////////////////////////////////////////////////////////

class SequenceFile : public FastaFile, public PhylipFile, public NexusFile {
public:

    enum Type {
        UNKNOWN, PHYLIP, NEXUS, FASTA
    };

    explicit SequenceFile(const std::string &newFilePath, Type newFileType);

    bool exists() override {
        return FastaFile::exists();
    };

    virtual const std::string &getPath() {
        return FastaFile::getPath();
    };

    virtual void setFileType(Type newType) {
        assert(fileType == UNKNOWN);
        fileType = newType;
    };

    std::vector<SequencePtr> read() override;

    void write(const Alignment &alignment) override;


private:
    Type fileType;

    void detectFileType();
};


#endif    /* SEQUENCEFILE_H */

