/**
 * Copyright (c) 2006-09 Maciej F. Boni.  All Rights Reserved.
 * 
 * FILE NAME:   SequenceFile.cpp
 * CREATED ON:  03 August 2011, 15:33
 * AUTHOR:      Maciej F. Boni, Ha Minh Lam
 * 
 * DESCRIPTION: Implement methods to read/write data from/into sequence files.
 *              Currently support Fasta, Phylip, and Nexus files.
 *              Multiple segments in one file have not been supported yet.
 * 
 * HISTORY:     Version    Date         Description
 *              1.0        2011-08-03   created
 * 
 * VERSION:     1.0
 * LAST EDIT:   03 August 2011
 */

#include "SequenceFile.h"
#include "../ui/Interface.h"

#include <stdexcept>


SequenceFile::SequenceFile(const std::string &newFilePath, Type newFileType)
        : FastaFile(newFilePath), PhylipFile(newFilePath),
          NexusFile(newFilePath), fileType(newFileType) {
}


void SequenceFile::detectFileType() {
    /* This method should not be called if the file type has been known */
    if (fileType != UNKNOWN) {
        return;
    }

    std::string firstString;

    /* Let assume this is a Fasta file to get rid
     * of the complexity of the multiple inheritance. */
    assert(FastaFile::exists());
    FastaFile::openToRead();

    std::fstream *fileStream = FastaFile::getStreamPtr();
    (*fileStream) >> firstString;

    if (FastaFile::isStreamReadable()) {
        if (firstString == "#NEXUS") {
            fileType = NEXUS;
        } else if (firstString[0] == '>') {
            fileType = FASTA;
        } else {
            std::string secondString;
            (*fileStream) >> secondString;
            if (FastaFile::isStreamReadable()
                && String::isInteger(firstString)
                && String::isInteger(secondString)) {
                fileType = PHYLIP;
            }
        }
    }

    FastaFile::close();
}


std::vector<SequencePtr> SequenceFile::read() {
    detectFileType();

    switch (fileType) {
        case FASTA:
            return FastaFile::read();

        case PHYLIP:
            return PhylipFile::read();

        case NEXUS:
            return NexusFile::read();

        default:
            throw std::runtime_error("Cannot detect sequence file format.");
    }
}


void SequenceFile::write(const Alignment &alignment) {
    switch (fileType) {
        case FASTA:
            FastaFile::write(alignment);
            return;

        case PHYLIP:
            PhylipFile::write(alignment);
            return;

        case NEXUS:
            NexusFile::write(alignment);
            return;

        default:
            Interface::instance() << "Cannot detect output file format.\n";
            Interface::instance().showError(true, true); // show error and exit
            return;
    }
}



////////////////////////////////////////////////////////////////////////////////
//  FASTA FILE
////////////////////////////////////////////////////////////////////////////////

FastaFile::FastaFile(const std::string &newFilePath) : TextFile(newFilePath) {
    // Do nothing
}


std::vector<SequencePtr> FastaFile::read() {
    std::vector<SequencePtr> sequenceList;

    assert(exists());
    openToRead();

    unsigned long seqCount = 0;
    std::string seqName;
    std::string seqString;

    // Read through each line of the file
    while (isStreamReadable()) {
        std::string line = String::trim(getLine());

        // If reach end of file or a new seqName number is found
        if (!isStreamReadable() || (line.length() > 0 && line[0] == '>')) {
            if (line.length() > 0 && line[0] != '>') {
                // This means reaching the end of file
                seqString += String::deleteAllSpace(line);
            }

            // If the previous sequence has not been stored into the alignment
            if (seqString.length() > 0) {
                /* Check if its seqName is valid */
                if (seqName.length() <= 0) {
                    Interface::instance() << "Invalid seqName at sequence "
                                          << seqCount + 1 << " in file "
                                          << getPath() << ".\n";
                    Interface::instance().showError(true, true);
                }
                auto sequence = Sequence::create(seqName, seqString);
                sequenceList.push_back(sequence);
            }

            if (line.length() > 0 && line[0] == '>') {
                seqName = line.substr(1, line.length() - 1);
                seqString = "";
            }

        } else {
            seqString += String::deleteAllSpace(line);
        }
    };

    close();
    return sequenceList;
}


void FastaFile::write(const Alignment &alignment) {
    if (exists()) {
        Interface::instance()
                << "The file \"" << getPath() << "\" has already existed.\n"
                << "Do you want to overwrite it?";
        if (!Interface::instance().showWarning(false)) {
            /* The program will be exit if the user say "No" */
            Interface::instance().throwExitSignal(0);

        } else {
            removeFile();
        }
    }

    openToWrite();
    write(alignment, getStreamPtr());
    close();
}


void FastaFile::write(const Alignment &alignment,
                      std::ostream *streamPtr) {
    auto sequenceList = alignment.getAllUsedSequences();
    for (const auto &sequence: sequenceList) {
        (*streamPtr) << ">" << sequence->getName() << std::endl
                     << sequence->toString() << std::endl;

    }
    (*streamPtr) << std::endl;
}



////////////////////////////////////////////////////////////////////////////////
//  PHYLIP FILE
////////////////////////////////////////////////////////////////////////////////

PhylipFile::PhylipFile(const std::string &newFilePath) : TextFile(newFilePath) {

}

std::vector<SequencePtr> PhylipFile::read() {
    // FIXME: cannot read Phylip interleaved files.

    std::vector<SequencePtr> sequenceList;

    assert(exists());
    openToRead();
    std::fstream *fStreamPtr = getStreamPtr();

    unsigned long seqNum = 0;
    unsigned long seqLength = 0;

    (*fStreamPtr) >> seqNum;
    (*fStreamPtr) >> seqLength;

    /* Now read in sequence name and nt-sequences seqNum times */
    unsigned long seqCount = 0;
    while (isStreamReadable()) {
        std::string seqName;
        std::string seqString;
        (*fStreamPtr) >> seqName;
        (*fStreamPtr) >> seqString;

        if (seqName.length() <= 0 && seqString.length() <= 0) {
            continue;
        }

        if (seqString.length() != seqLength) {
            Interface::instance() << "Sequence " << seqName
                                  << " has length " << seqString.length()
                                  << ".  Should be " << seqLength << ".\n";
            Interface::instance().showError(true, true); // show error & exit
        }

        auto sequence = Sequence::create(seqName, seqString);
        sequenceList.push_back(sequence);
        seqCount++;
    }
    close();

    if (seqCount != seqNum) {
        Interface::instance() << "Expecting " << seqNum
                              << " sequences. Only " << seqCount
                              << " found.\n";
        Interface::instance().showError(true, true); // show error & exit
    }

    return sequenceList;
}


void PhylipFile::write(const Alignment &alignment) {
    if (exists()) {
        Interface::instance()
                << "The file \"" << getPath() << "\" has already existed.\n"
                << "Do you want to overwrite it?";
        if (!Interface::instance().showWarning(false)) {
            /* The program will be exit if the user say "No" */
            Interface::instance().throwExitSignal(0);

        } else {
            removeFile();
        }
    }

    openToWrite();
    write(alignment, getStreamPtr());
    close();
}


void PhylipFile::write(const Alignment &alignment, std::ostream *streamPtr) {
    auto sequenceList = alignment.getAllUsedSequences();
    (*streamPtr) << "  " << sequenceList.size()
                 << "  " << alignment.activeLength() << std::endl;

    for (const auto &sequence: sequenceList) {
        (*streamPtr) << sequence->getName() << "\t"
                     << sequence->toString() << std::endl;

    }
    (*streamPtr) << std::endl;
}



////////////////////////////////////////////////////////////////////////////////
//  NEXUS FILE
////////////////////////////////////////////////////////////////////////////////

NexusFile::NexusFile(const std::string &newFilePath) : TextFile(newFilePath) {

}


std::vector<SequencePtr> NexusFile::read() {
    //TODO: Read Nexus file
    throw std::runtime_error("Reading NEXUS file has not been supported yet.");
}


void NexusFile::write(const Alignment &alignment) {
    if (exists()) {
        Interface::instance()
                << "The file \"" << getPath() << "\" has already existed.\n"
                << "Do you want to overwrite it?";
        if (!Interface::instance().showWarning(false)) {
            /* The program will be exit if the user say "No" */
            Interface::instance().throwExitSignal(0);

        } else {
            removeFile();
        }
    }

    openToWrite();
    write(alignment, getStreamPtr());
    close();
}


void NexusFile::write(const Alignment &alignment,
                      std::ostream *streamPtr) {
    auto sequenceList = alignment.getAllUsedSequences();

    (*streamPtr)
            << "#NEXUS\n\n[!Data from\n\n]\n\nbegin taxa;\n\tdimensions ntax="
            << sequenceList.size()
            << ";\n\ttaxlabels";

    for (const auto &sequence: sequenceList) {
        (*streamPtr) << "\n\t\t" << sequence->getName();
    }

    (*streamPtr)
            << ";\nend;\n\nbegin characters;\n\tdimensions nchar="
            << alignment.activeLength()
            << ";\n\tformat missing=? gap=- matchchar=. datatype=dna;\n\toptions gapmode=missing;\n\tmatrix\n";

    for (const auto &sequence: sequenceList) {
        (*streamPtr) << "\n" << sequence->getName() << "    \t" << sequence->toString();
    }

    (*streamPtr) << ";\nend;\n\n";
}
