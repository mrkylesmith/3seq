/* 
 * File:   VersionRun.h
 * Author: Maciej F. Boni, Ha Minh Lam
 *
 * Created on 17 August 2011, 11:57
 */

#ifndef VERSION_RUN_H
#define	VERSION_RUN_H

#include <cassert>
#include "Run.h"
#include "../ui/Interface.h"

class VersionRun : public Run {
public:
    explicit VersionRun(int argc, char** argv);

    ~VersionRun() override;

    bool isLogFileSupported() const override {
        return false;
    };

    Mode getMode() const override {
        return VERSION;
    };
    
    void parseCmdLine() override;

    void perform() override;

    VersionRun(const VersionRun& orig) = delete;

    VersionRun& operator=(const VersionRun& rhs) = delete;


};

#endif	/* VERSION_RUN_H */

