/* 
 * File:   HelpRun.h
 * Author: Maciej F. Boni, Ha Minh Lam
 *
 * Created on 12 August 2011, 17:08
 */

#ifndef HELPRUN_H
#define	HELPRUN_H

#include <cassert>
#include "Run.h"

//class Run;

class HelpRun : public Run {
public:

    explicit HelpRun(int argc, char** argv);

    HelpRun(const HelpRun& orig) = delete;

    HelpRun& operator=(const HelpRun& rhs) = delete;

    ~HelpRun() override = default;

    bool isLogFileSupported() const override {
        return false;
    };

    Mode getMode() const override {
        return HELP;
    };

    void parseCmdLine() override;

    void perform() override;

};

#endif	/* HELPRUN_H */

