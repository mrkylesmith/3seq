/**
 * Copyright (c) 2006-09 Maciej F. Boni.  All Rights Reserved.
 *
 * FILE NAME:   FullRun.cpp
 * CREATED ON:  17 August 2011
 * AUTHOR:      Maciej F. Boni, Ha Minh Lam
 *
 * DESCRIPTION: See "FullRun.h"
 *
 * HISTORY:     Version     Date        Description
 *              1.0         2011-08-17  created
 *
 * VERSION:     1.0
 * LAST EDIT:   17 August 2011
 */

#include "FullRun.h"


FullRun::FullRun(int argc, char **argv)
        : Run(argc, argv),
          tripletPool(), alignmentDescriptor(this->alignment) {
    Interface::instance().startProgram("Full Run");

    fileSkippedTriplets = nullptr;
    fileRecombinants = nullptr;
    fileLongRecombinants = nullptr;
}

FullRun::~FullRun() {
    delete fileSkippedTriplets;
    delete fileRecombinants;
    delete fileLongRecombinants;
}

void FullRun::parseCmdLine() {
    std::vector<SequencePtr> parentSeqs;
    std::vector<SequencePtr> childSeqs;

    int argNum = getRunArgsNum();

    if (argNum < 1) {
        Interface::instance() << "Not enough parameter for full run.\n";
        Interface::instance().showError(true, true);
    }

    string filePath = argVector[2];
    SequenceFile parentFile(filePath, SequenceFile::UNKNOWN);
    if (!parentFile.exists()) {
        Interface::instance() << "File \"" << filePath << "\" not found.\n";
        Interface::instance().showError(true, true);
    }
    parentSeqs = parentFile.read();

    if (argNum >= 2) {
        argNum = 2;
        filePath = argVector[3];
        SequenceFile childFile(filePath, SequenceFile::UNKNOWN);
        if (!childFile.exists()) {
            Interface::instance() << "File \"" << filePath << "\" not found.\n";
            Interface::instance().showError(true, true);
        }
        childSeqs = childFile.read();
    }

    if (childSeqs.empty()) {
        alignment.addSequences(parentSeqs, true, true);
    } else {
        alignment.addSequences(parentSeqs, true, false);
        alignment.addSequences(childSeqs, false, true);
    }

    isRandomMode = false;
    randomSeqNum = 0;
    randomLoopNum = 1;
    pTableFile = nullptr;

    for (int i = 2 + argNum; i < argCount - 1; i++) {
        string arg = argVector[i];
        if (arg == "-ptable" || arg == "-p") {
            string pTableFilePath = argVector[i + 1];
            pTableFile = new PTableFile(pTableFilePath);
            argVector[i] = "";
            argVector[i + 1] = "";

        } else if (arg == "-rand") {
            errno = 0;
            randomSeqNum = strtoul(argVector[i + 1].c_str(), nullptr, 0);
            if (errno || randomSeqNum <= 0) {
                Interface::instance() << "Cannot convert \"" << argVector[i + 1]
                                      << "\" to a positive number\n";
                Interface::instance().showError(true, true);
            }
            argVector[i] = "";
            argVector[i + 1] = "";
            isRandomMode = true;

        } else if (arg == "-n") {
            errno = 0;
            randomLoopNum = strtoul(argVector[i + 1].c_str(), nullptr, 0);
            if (errno || randomLoopNum <= 0) {
                Interface::instance() << "Cannot convert \"" << argVector[i + 1]
                                      << "\" to a positive number\n";
                Interface::instance().showError(true, true);
            }
            argVector[i] = "";
            argVector[i + 1] = "";
            isRandomMode = true;
        }
    }

    if (pTableFile != nullptr && !pTableFile->exists()) {
        Interface::instance() << "The file \"" << pTableFile->getFilePath()
                              << "\" does not exist.\n";
        Interface::instance().showError(true, true);
    }

    deleteCmdLineArgs(argNum + 1); // --fullrun inputFile ...
    Run::parseCmdLine(); // Get common run options
}

void FullRun::perform() {
    // Process common data. The alignment is also modified (cut, shrink, etc.) at this step.
    Run::perform();

    verifyData();
    setup();
    loadPTable(pTableFile);


    Interface::instance() << Interface::SEPARATOR << endl;
    Interface::instance().showLog(true);

    progress();

    tripletPool.writeToFile(fileRecombinants);

    Interface::instance() << Interface::SEPARATOR << endl;
    Interface::instance().showLog(true);

    if (isRandomMode && randomLoopNum > 1) {
        Interface::instance() << "Recombination sensitivity: "
                              << recombinationSensitivity << "/"
                              << randomLoopNum << "\n";
        Interface::instance().showOutput(true);

    } else {
        displayResult();
        Interface::instance() << Interface::SEPARATOR << endl;
        Interface::instance().showLog(true);
        savePValHistogram('\t');
    }


    /* Close all files */
    if (fileSkippedTriplets)
        fileSkippedTriplets->close();
    if (fileRecombinants)
        fileRecombinants->close();
    if (fileLongRecombinants != nullptr)
        fileLongRecombinants->close();
}

void FullRun::verifyData() {
    // TODO: re-implement RANDOM MODE
//    if (isRandomMode) {
//        cloStopAtFirstRecombinant = true;
//
//        if (cloBeginSequence > 0 || cloEndSequence < alignment.getChildPool().countAllSequences())) {
//            // TODO: re-test this part
//            Interface::instance()
//                    << "-b and -e options cannot be used with random sub-sampling.\n";
//            Interface::instance().showError(true, true);
//        }
//
//        if (!alignment.isChildParentFromSamePool()) {
//            Interface::instance()
//                    << "Parent and child sequences are from different datasets.\n"
//                    << "Only the child dataset will be sub-sampled.\n";
//            Interface::instance().showWarning(true);
//        }
//
//        if (cloUseAllTripletsForStatCorrection) {
//            Interface::instance() << ""
//                                     "Auto-enabled -# option for random sub-sampling. Actual number of\n"
//                                     "comparisons will be used in corrections.";
//            Interface::instance().showWarning(true);
//            cloUseAllTripletsForStatCorrection = false;
//        }
//
//        alignment.getChildPool()->randomlyActivate(randomSeqNum);
//    }

    /* Calculate the number of actual comparisons */
//    double totalTripletNum = getNumTotalTriplets();

//    if (totalTripletNum <= 0) {
//        Interface::instance()
//                << "The number of sequences is not enough (at least 3 sequences needed).\n";
//        Interface::instance().showError(true, true);
//    } else if (totalTripletNum > MAX_UL) {
//        Interface::instance()
//                << "The number of comparisons you are requesting is larger than LONG_MAX for an\n"
//                << "\"unsigned long\" type on your system. The P-values below should be correct,\n"
//                << "but the numbers of comparisons that were approximated/computed/skipped might\n"
//                << "not be the exact values.";
//        Interface::instance().showWarning();
//    }

    /* Show some logs */
    Interface::instance()
            << "Using " << alignment.getParentPool().countActiveSequences()
            << " sequences as parents.\n"
            << "Using " << alignment.getChildPool().countActiveSequences()
            << " sequences as children.\n";
    Interface::instance().showLog(true);
}

void FullRun::setup() {
    Triplet::setAcceptApproxPVal(!cloNoSiegmundApprox);
    Triplet::setLongRecombinantThreshold(cloMinLongRecombinantLength);

    this->numTripletsForStatCorrection = alignmentDescriptor.getTripletCounts()->active;
    if (cloUseAllTripletsForStatCorrection) {
        this->numTripletsForStatCorrection = alignmentDescriptor.getTripletCounts()->all;
    }
    stat::correction::setSampleNumForPValCorrection(numTripletsForStatCorrection);
    Interface::instance() << "Need a p-value of "
                          << cloRejectThreshold / numTripletsForStatCorrection
                          << " to survive multiple comparisons correction.\n";
    Interface::instance().showLog(true);

    if (cloAllBpCalculated) {
        tripletPool.setStorageMode(TripletPool::StorageMode::ALL_TRIPLETS);
    } else {
        tripletPool.setStorageMode(TripletPool::StorageMode::BEST_TRIPLET);
    }

    if (!isRandomMode) {
        randomLoopNum = 1;
    } else {
        cloNoBpCalculated = true;
        cloNo3sRecFile = true;
    }

    /* Prepare file names */
    string recombinantsFileName = config::DEFAULT_RECOMBINANTS_FILE_NAME;
    string longRecombinantsFileName = config::DEFAULT_LONG_RECOMBINANTS_FILE_NAME;
    string skippedTripletFileName = config::DEFAULT_SKIPPED_TRIPLETS_FILE_NAME;
    if (id.length() > 0) {
        const string period = ".";
        recombinantsFileName = id + period + recombinantsFileName;
        longRecombinantsFileName = id + period + longRecombinantsFileName;
        skippedTripletFileName = id + period + skippedTripletFileName;
    }

    /* 3s.skipped */
    fileSkippedTriplets = nullptr;
    if (cloIsSkippedTriplesRecorded) {
        fileSkippedTriplets = new TextFile(skippedTripletFileName);

        Interface::instance()
                << "Skipped triplets will be recorded to the file \""
                << fileSkippedTriplets->getPath() << "\".\n";

        if (fileSkippedTriplets->exists()) {
            Interface::instance()
                    << "This file already exists. Do you want to overwrite it?";
            if (!Interface::instance().showWarning(false)) {
                /* User says "No" */
                delete fileSkippedTriplets;
                fileSkippedTriplets = nullptr;
                Interface::instance()
                        << "Skipped triplets will not be saved.\n";
                Interface::instance().showLog(true);
            } else {
                fileSkippedTriplets->removeFile();
            }

        } else {
            Interface::instance().showLog(true);
        }
    }

    /* 3s.rec */
    fileRecombinants = nullptr;
    if (!cloNo3sRecFile) {
        fileRecombinants = new TextFile(recombinantsFileName);

        Interface::instance()
                << "All recombinant triplets will be recorded to the file \""
                << fileRecombinants->getPath() << "\".\n";

        if (fileRecombinants->exists()) {
            Interface::instance()
                    << "This file already exists. Do you want to overwrite it?";
            if (!Interface::instance().showWarning(false)) {
                /* User says "No" */
                delete fileRecombinants;
                fileRecombinants = nullptr;
                Interface::instance()
                        << "Recombinant triplets will not be saved.\n";
                Interface::instance().showLog(true);
            } else {
                fileRecombinants->removeFile();
            }

        } else {
            Interface::instance().showLog(true);
        }
    }

    /* 3s.longRec */
    fileLongRecombinants = new TextFile(longRecombinantsFileName);
    Interface::instance()
            << "Long recombinants will be recorded to the file \""
            << fileLongRecombinants->getPath() << "\".\n";

    if (fileLongRecombinants->exists()) {
        Interface::instance()
                << "This file already exists. Do you want to overwrite it?";
        if (!Interface::instance().showWarning(false)) {
            /* User says "No" */
            delete fileLongRecombinants;
            fileLongRecombinants = nullptr;
            Interface::instance() << "Long Recombinants will not be saved.\n";
            Interface::instance().showLog(true);
        } else {
            fileLongRecombinants->removeFile();
        }

    } else {
        Interface::instance().showLog(true);
    }
}

void FullRun::showProgress(double currentLoop, bool isFinish) const {
    char strBuf[200];

    if (cloAllBpCalculated) {
        sprintf(strBuf,
                "     %s       %e       %10.0llu            %lu",
                Interface::instance().getElapsedTime().c_str(),
                minPVal, numRecombinantTriplets,
                tripletPool.getLongestMinRecLength());
    } else if (cloStopAtFirstRecombinant) {
        std::string recFound = "NO";
        if (numRecombinantTriplets > 0) {
            recFound = "YES";
        }
        sprintf(strBuf,
                "     %s       %e       %6s",
                Interface::instance().getElapsedTime().c_str(),
                minPVal, recFound.c_str());
    } else {
        sprintf(strBuf,
                "     %s       %e       %10.0llu",
                Interface::instance().getElapsedTime().c_str(),
                minPVal, numRecombinantTriplets);
    }
    Interface::instance() << strBuf;

    if (isFinish) {
        Interface::instance().finishCounting();
    } else {
        Interface::instance().count(currentLoop);
    }
}

void FullRun::progress() {
    if (cloAllBpCalculated) {
        Interface::instance()
                << "                                                Recombinant      Longest Recombinant\n"
                << "Progress    Time Elapsed    Minimum P-Value    Triplets Found          Segment\n";
    } else {
        Interface::instance()
                << "                                                Recombinant\n"
                << "Progress    Time Elapsed    Minimum P-Value    Triplets Found\n";
    }
    Interface::instance().showLog(false);

    recombinationSensitivity = 0;

    for (unsigned long randLoopCounter = 0;
         randLoopCounter < randomLoopNum; randLoopCounter++) {

        // TODO: re-implement the random mode
//        if (isRandomMode) {
//            childDataset->randomlyActivate(randomSeqNum);
//            calculateTripletNum();
//        }

        numRecombinantTriplets = 0;
        numComputedExactly = 0;
        numApproximated = 0;
        numSkipped = 0;

        minPVal = 1.0;

        /* Initialise progress counter */
        auto childSequences = alignment.getActiveChildren();
        auto parentSequences = alignment.getActiveParents();

        unsigned long activeChildNum = childSequences.size();
        unsigned long activeParentNum = parentSequences.size();
        double totalOuterLoops = static_cast<double> (activeChildNum)
                                 * static_cast<double> (activeParentNum);
        Interface::instance().initCounter("", 0, totalOuterLoops);


        /* This flag indicates if the progressing should be stopped */
        bool isStopped = false;

        /* Progressing begins */
        time_t lastTime =
                time(nullptr) - config::PROGRESS_MONITOR_UPDATE_RATE - 1;

        double performedOuterLoops = 0.0;
        for (const auto &child: childSequences) {
            if (isStopped) { break; }

            for (const auto &dad: parentSequences) {
                if (isStopped) { break; }
                if (dad == child) {
                    continue;
                }
                performedOuterLoops += 1.0;

                /* Show progress counter */
                time_t currentTime = time(nullptr);
                if (currentTime - lastTime >= config::PROGRESS_MONITOR_UPDATE_RATE) {
                    showProgress(performedOuterLoops, false);
                    lastTime = currentTime;
                }

                for (const auto &mum: parentSequences) {
                    if (isStopped) { break; }
                    if (mum == dad || mum == child) {
                        continue;
                    }

                    auto triplet = tripletPool.newTriplet(child, dad, mum);

                    /* Let's see if we can calculate the exact P-value */
                    if (!triplet->hasPVal()) {
                        if (fileSkippedTriplets) {
                            /* No need to open file, writeLine() will do that automatically */
                            fileSkippedTriplets->writeLine(triplet->describe());
                        }
                        numSkipped++;
                        continue;
                    }

                    if (triplet->hasExactPVal()) {
                        numComputedExactly++;
                    } else {
                        numApproximated++;
                    }

                    double pValue = triplet->getPValue();
                    addPValIntoHistogram(pValue);
                    if (pValue < minPVal) {
                        minPVal = pValue;
                    }

                    /* Let's see if the P-value is significant */
                    auto correctedPVal = stat::correction::dunnSidak(
                            pValue, numTripletsForStatCorrection);
                    if (correctedPVal < cloRejectThreshold) {
                        numRecombinantTriplets++;
                        if (child->getRecombinantType() == Sequence::RecombinantType::NOT_RECOM) {
                            child->setRecombinantType(
                                    Sequence::RecombinantType::RECOMBINANT);
                        }
                        tripletPool.saveTriplet(child, triplet);

//                        /* Go into this loop either if we're doing ALL breakpoint or
//                         * NO breakpoint if we are doing no breakpoint, then the
//                         * breakpoint calculations is skipped. */
//                        if (cloAllBpCalculated || cloNoBpCalculated) {
//                            recordRecombinantTriplet(triplet);
//                        }
//
//                        /* Now check if this is the *best* recombinant (meaning lowest p-value)
//                         * and if it is, record it */
//                        if (child->getBestRecombinantTriplet() == nullptr
//                            || pValue <
//                               child->getBestRecombinantTriplet()->getPVal()) {
//                            child->setBestRecombinantTriplet(triplet);
//                        } else {
//                            delete triplet;
//                        }


                        if (cloStopAtFirstRecombinant) {
                            /* Finish progressing early */
                            isStopped = true;
                        }

                    } else {
                        tripletPool.freeTriplet(triplet);
                    }
                }
            }

            if (!cloNoBpCalculated) {
                tripletPool.seekBreakPointPairs(child);
            }
        }

        /* Progressing finished */
        showProgress(0.0, true);

        if (numRecombinantTriplets > 0) {
            recombinationSensitivity++;
        }
    }

    Interface::instance().showLog(true);
}

//void FullRun::recordRecombinantTriplet(TripletPtr triplet, const char &separator) {
//    if (!cloNoBpCalculated) {
//        if (triplet->getMinRecLength() >= cloMinLongRecombinantLength)
//            triplet->getChild()->setRecombinantType(Genome::LONG_REC);
//
//        if (triplet->getMinRecLength() > tripletPool.getLongestMinRecLength())
//            tripletPool.getLongestMinRecLength() = triplet->getMinRecLength();
//    }
//
//    /* Begin writing 3s.rec file */
//    if (fileRecombinants == nullptr) return;
//
//    /* Write the header */
//    if (!fileRecombinants_hasHeader) {
//        fileRecombinants->writeLine(
//                string("P_ACCNUM") + separator +
//                string("Q_ACCNUM") + separator +
//                string("C_ACCNUM") + separator +
//                'm' + separator +
//                'n' + separator +
//                'k' + separator +
//                'p' + separator +
//                string("HS?") + separator +
//                string("log(p)") + separator +
//                string("DS(p)") + separator +
//                string("DS(p)") + separator +
//                string("min_rec_length") + separator +
//                string("breakpoints")
//        );
//        fileRecombinants_hasHeader = true;
//    }
//
//    double pValue = triplet->getPVal();
//    long double dunnSidak = stat::correction::dunnSidak(pValue,
//                                                        getNumTripletsForCorrection());
//
//    char buff[1000];
//    sprintf(buff,
//            "%s%c%s%c%s%c%lu%c%lu%c%lu%c%1.12f%c%d%c%2.4f%c%1.5Lf%c%Le",
//            triplet->getDad()->getAccession().c_str(), separator,
//            triplet->getMum()->getAccession().c_str(), separator,
//            triplet->getChild()->getAccession().c_str(), separator,
//            triplet->getUpStep(), separator,
//            triplet->getDownStep(), separator,
//            triplet->getMaxDecent(), separator,
//            pValue, separator,
//            (!triplet->hasExactPVal()), separator,
//            log10(pValue), separator,
//            dunnSidak, separator,
//            dunnSidak);
//    fileRecombinants->write(buff);
//
//    if (!cloNoBpCalculated) {
//        sprintf(buff, "%c%lu", separator, triplet->getMinRecLength());
//        fileRecombinants->write(buff);
//
//        vector<Triplet::BreakPointPair> breakPointPairs = triplet->getBreakPointPairs();
//
//        for (unsigned long i = 0; i < breakPointPairs.size(); i++) {
//            Triplet::BreakPointPair bpPair = breakPointPairs[i];
//            fileRecombinants->write(separator + bpPair.describe());
//        }
//    }
//
//    fileRecombinants->writeLine();
//    /* Finish writing 3s.rec file */
//}


void FullRun::displayResult() {
    unsigned long numRecombinant = 0;
    unsigned long numLongRec = 0;

    for (const auto &child: alignment.getActiveChildren()) {
        if (child->getRecombinantType() != Sequence::RecombinantType::NOT_RECOM) {
            numRecombinant++;
        }
        if (child->getRecombinantType() == Sequence::RecombinantType::LONG_RECOM) {
            numLongRec++;
            if (fileLongRecombinants != nullptr) {
                fileLongRecombinants->writeLine(child->getName());
            }
        }
    }


    Interface::instance()
            << "Number of triples tested :              "
            << alignmentDescriptor.getTripletCounts()->active << "\n"
            << "Number of p-values computed exactly :   " << numComputedExactly
            << "\n"
            << "Number of p-values approximated (HS) :  " << numApproximated
            << "\n"
            << "Number of p-values not computed :       " << numSkipped << "\n"
            << endl
            << "Number of recombinant triplets :                               \t"
            << numRecombinantTriplets << "\n"
            << "Number of distinct recombinant sequences :                     \t"
            << numRecombinant << "\n";

    if (cloStopAtFirstRecombinant) {
        Interface::instance() << "[[ search stopped when first one found ]]\n"
                              << endl;
    }

    if (!cloNoBpCalculated) {
        Interface::instance()
                << "Number of distinct recombinant sequences at least "
                << cloMinLongRecombinantLength << "nt long : \t" << numLongRec
                << "\n"
                << "Longest of short recombinant segments :                        \t"
                << tripletPool.getLongestMinRecLength() << "nt\n";
    }
    Interface::instance().showOutput(true);

    Interface::instance() << Interface::SEPARATOR << endl;
    Interface::instance().showLog(true);

    char formatedPVal[20];
    sprintf(formatedPVal,
            "%1.3e",
            static_cast<double> (stat::correction::dunnSidak(minPVal)));
    Interface::instance()
            << "Rejection of the null hypothesis of clonal evolution at p = "
            << stat::correction::dunnSidak(minPVal)
            << "\n"
            << "                                                        p = "
            << formatedPVal << "\n"
            << "                                            Uncorrected p = "
            << minPVal << "\n"
            << "                                            Bonferroni  p = "
            << stat::correction::bonferroni(minPVal)
            << "\n";
    Interface::instance().showOutput(true);
}


//void FullRun::outputForPython() const {
//    printf("%1.4f\t%d\t%d\t%d\t%d\t%d",
//            correction::dunnSidak(minPVal, numForCorrection),
//            numComputedExactly,
//            numApproximated,
//            numSkipped,
//            parentDataset->getSize(),
//            numRecombinantTriplets
//            );
//}
