/**
 * Copyright (c) 2006-09 Maciej F. Boni.  All Rights Reserved.
 *
 * FILE NAME:   TestPRun.cpp
 * CREATED ON:  21 August 2011
 * AUTHOR:      Maciej F. Boni, Ha Minh Lam
 *
 * DESCRIPTION: This run-type is used to test P-values. After the user enters
 *              values for M, N, K, the program will return the corresponding
 *              P-value.
 *
 * NOTES:       This run-type should only work in CONSOLE mode (@see
 * Interface.h).
 *
 * HISTORY:     Version     Date        Description
 *              1.0         2011-08-21  Created
 *                          2012-04-18  Limit number of digits of M, N, K to
 *                                      avoid integer overflow.
 *
 * VERSION:     1.0
 * LAST EDIT:   18 April 2012
 */

#include "CheckPRun.h"

#include "../util/Util.h"

CheckPRun::CheckPRun(int argc, char **argv) : Run(argc, argv) {
	pTableFile = nullptr;

	//Interface::instance().startProgram("Test P-Values");
}

// CheckPRun::CheckPRun(const CheckPRun& orig) {
//     assert(false); // should never reach here
// }
//
// CheckPRun& CheckPRun::operator=(const CheckPRun& rhs) {
//     if (this != &rhs) {
//         assert(false); // should never reach here
//     }
//     return *this;
// }

CheckPRun::~CheckPRun() = default;

void CheckPRun::parseCmdLine() {
	if (getRunArgsNum() >= 1) {
		string pTableFilePath = argVector[2];
		pTableFile = new PTableFile(pTableFilePath);

		if (!pTableFile->exists()) {
			Interface::instance() << "The file \"" << pTableFilePath
					      << "\" does not exist.\n";
			Interface::instance().showError(true, true);
		}
	}
}

void CheckPRun::perform() {
	static const string PROMPT = "Enter M N K to test :  ";
	static const string VAL_SEPARATOR = "  ";
	static const int PARAMS_NUM = 3;

	vector<MNK> history;

	loadPTable(pTableFile);
	string lastBuf[PARAMS_NUM] = {"", "", ""};
	string currentBuf[PARAMS_NUM] = {"", "", ""};
	int bufCursor = 0;
	unsigned long historyCursor = history.size();

	cout << PROMPT;
	ofstream mnk("data/mnk.log");
	fstream trios;
	trios.open("data/mnk_no_dups.txt");

	string line;

	while (getline(trios, line)) {
		// Get M N K trios as input
		mnk << PROMPT;

		istringstream ss(line);
		string token;

		std::vector<string> v;
		// Split line by spaces
		while (std::getline(ss, token, ' ')) {
			v.push_back(token);
		}
		// M N K trios to get pvals
		string m_value = v[0];
		string n_value = v[1];
		string k_value = v[2];

		mnk << m_value << "  " << n_value << "  " << k_value << "\n";
		// Add new M N K trios
		MNK newMnk;
		newMnk.m = stoi(m_value);
		newMnk.n = stoi(n_value);
		newMnk.k = stoi(k_value);
		history.push_back(newMnk);

		unsigned long historySize = history.size();
		if (historySize > 0) {
			MNK lastMNK = history[historySize - 1];
			int m = lastMNK.m;
			int n = lastMNK.n;
			int k = lastMNK.k;

			if (PTable::instance().canCalculateExact(m, n, k)) {
				mnk << "P-value [" << m << ", " << n << ", "
				    << k << "]  =  "
				    << PTable::instance().getExactPValue(m, n,
									 k)
				    << endl;
			} else {
				mnk << "P-value [" << m << ", " << n << ", "
				    << k << "]  ~  "
				    << PTable::instance().approxPValue(m, n, k)
				    << "  (approximated, H-S discrete)" << endl;
			}
			mnk << endl;
		}
	}
	mnk.close();
	trios.close();
}

