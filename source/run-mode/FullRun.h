/**
 * Copyright (c) 2006-09 Maciej F. Boni.  All Rights Reserved.
 *
 * FILE NAME:   FullRun.h
 * CREATED ON:  17 August 2011
 * AUTHOR:      Maciej F. Boni, Ha Minh Lam
 *
 * DESCRIPTION: 
 *
 * HISTORY:     Version     Date        Description
 *              1.0         2011-08-17  created
 *
 * VERSION:     1.0
 * LAST EDIT:   17 August 2011
 */

#ifndef FULLRUN_H
#define	FULLRUN_H

#include <cassert>
#include <string>
#include "Run.h"
#include "../file/SequenceFile.h"
#include "../stat/PTableFile.h"
#include "../bio/Triplet.h"
#include "../bio/TripletPool.h"
#include "../bio/AlignmentDescriptor.h"


class FullRun : public Run {
public:
    explicit FullRun(int argc, char** argv);

    virtual ~FullRun();

    virtual bool isLogFileSupported() const {
        return true;
    };
    
    virtual Mode getMode() const {
        return FULL;
    };
    
    virtual void parseCmdLine();
    
    virtual void perform();


private:
    FullRun(const FullRun& orig) = delete;

    FullRun& operator=(const FullRun& rhs) = delete;

    /**
     * Test the dataset, and show some info about the dataset.
     */
    void verifyData();

    /**
     * Prepare for the run.
     */
    void setup();

    /**
     * Run the analysis. This is the core of "full-run".
     */
    void progress();

    /**
     * Show the progress counter.
     * @param currentLoop
     * @param isFinish
     */
    void showProgress(double currentLoop, bool isFinish) const;

    /**
     * Show the result of the analysis.
     */
    void displayResult();

//    void recordRecombinantTriplet(TripletPtr triplet, const char& separator = ',');

    TripletPool tripletPool;
    
    AlignmentDescriptor alignmentDescriptor;

    /** P-value table file */
    PTableFile* pTableFile;
    
    /** Minimum P-value that has been reached during this run */
    double minPVal;

    /**
     * Number of triplets to be used for Dunn-Sidak and Bonferroni corrections
     */
    unsigned long long numTripletsForStatCorrection;

    /**
     * Number of triplets where recombinations have been detected
     * (significant P-values were returned).
     */
    unsigned long long numRecombinantTriplets;
    
    /** Number of triplets where P-values are computed exactly */
    unsigned long long numComputedExactly;
       
    /** Number of triplets that where P-values were approximated */
    unsigned long long numApproximated;
    
    /**
     * Number of triplets that have been skipped during the analysis
     * (due to P-value table out-of-bound & no use of approximation).
     */
    unsigned long long numSkipped;

    TextFile* fileSkippedTriplets;
    TextFile* fileLongRecombinants;
    TextFile* fileRecombinants;

    /** Number of random sequences in each random loop */
    unsigned long randomSeqNum;
    
    /** Number of random iteration */
    unsigned long randomLoopNum;
    
    /** Indicates the current run is in random mode. */
    bool isRandomMode;   
    
    /**
     * This variable is only used in random mode to indicate the number of
     * random iterations where there is at least 1 recombination event detected.
     */
    unsigned long recombinationSensitivity;
};

#endif	/* FULLRUN_H */

