/* 
 * File:   WriteRun.h
 * Author: Maciej F. Boni, Ha Minh Lam
 *
 * Created on 19 August 2011, 16:26
 */

#ifndef WRITE_RUN_H
#define	WRITE_RUN_H

#include <cassert>
#include <string>
#include <iostream>
#include "Run.h"
#include "../file/SequenceFile.h"

class WriteRun : public Run {
public:
    explicit WriteRun(int argc, char** argv);

    ~WriteRun() override;

    bool isLogFileSupported() const override {
        return false;
    };

    Mode getMode() const override {
        return WRITE;
    };

    void parseCmdLine() override;

    void perform() override;

    WriteRun(const WriteRun& orig) = delete;

    WriteRun& operator=(const WriteRun& rhs) = delete;


private:
    SequenceFile* outputFile;
};

#endif	/* WRITE_RUN_H */

