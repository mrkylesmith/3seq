/**
 * Copyright (c) 2006-09 Maciej F. Boni.  All Rights Reserved.
 *
 * FILE NAME:   InfoRun.cpp
 * CREATED ON:  18 August 2011
 * AUTHOR:      Maciej F. Boni, Ha Minh Lam
 *
 * DESCRIPTION:
 *
 * HISTORY:     Version     Date        Description
 *              1.0         2011-08-18  created
 *
 * VERSION:     1.0
 * LAST EDIT:   18 August 2011
 */

#include "InfoRun.h"
#include "../util/numeric_types.h"
#include "../bio/AlignmentDescriptor.h"


InfoRun::InfoRun(int argc, char **argv) : Run(argc, argv) {
    Interface::instance().startProgram("Info Mode");
}


InfoRun::~InfoRun() = default;


void InfoRun::parseCmdLine() {
    if (getRunArgsNum() < 1) {
        Interface::instance() << "Not enough parameter for info-run mode.\n";
        Interface::instance().showError(true, true);
    }

    string filePath = argVector[2];

    SequenceFile inputFile(filePath, SequenceFile::UNKNOWN);
    if (!inputFile.exists()) {
        Interface::instance() << "File \"" << filePath << "\" not found.\n";
        Interface::instance().showError(true, true);

    }

    auto sequenceList = inputFile.read();
    this->alignment.addSequences(sequenceList, true, true);
    this->alignment.lock();

    deleteCmdLineArgs(2); // --info inputFile
    Run::parseCmdLine(); // Get common run options
}


void InfoRun::perform() {
    Run::perform(); // process common data

    AlignmentDescriptor alignmentDescriptor(alignment);

    Interface::instance()
            << "Number of sequences          :  "
            << alignment.countAllSequences() << "\n"
            << "Number of distinct sequences :  "
            << alignment.countDistinctSequences() << "\n"
            << "Number of triplet comparisons:  "
            << alignmentDescriptor.getTripletCounts()->active << "\n";
    Interface::instance().showOutput(true);

    Interface::instance()
            << "Original sequence length is " << alignment.fullLength() << " nt.\n";

    if (isSet(cloFirstNucleotidePos) && isSet(cloLastNucleotidePos)) {
        if (cloIsFirstToLastCutOut) {
            Interface::instance()
                    << "Cutting out " << cloLastNucleotidePos - cloFirstNucleotidePos + 1
                    << " positions, from " << cloFirstNucleotidePos
                    << " to " << cloLastNucleotidePos << " inclusive.\n";

        } else {
            Interface::instance()
                    << "Using " << cloLastNucleotidePos - cloFirstNucleotidePos + 1
                    << " positions, from " << cloFirstNucleotidePos
                    << " to " << cloLastNucleotidePos << " inclusive.\n";
        }
    }

    if (cloUseAllSites) {
        Interface::instance()
                << endl
                << "Using all " << alignment.activeLength() << " sites.\n"
                << "Counting gap/nucleotide mismatches as differences.\n"
                << "    gapped: " << alignmentDescriptor.getAllelicStats()->nOnlyGaps
                << " - mono: " << alignmentDescriptor.getAllelicStats()->nMonoallelic
                << " - bi: " << alignmentDescriptor.getAllelicStats()->nBiallelic
                << " - tri: " << alignmentDescriptor.getAllelicStats()->nTriallelic
                << " - tetra: " << alignmentDescriptor.getAllelicStats()->nTetrallelic << "\n";

    } else {
        Interface::instance()
                << endl
                << "Number of polymorphic sites is " << alignment.activeLength()
                << ". Using only polymorphic sites:\n"
                << "    bi: " << alignmentDescriptor.getAllelicStats()->nBiallelic
                << " - tri: " << alignmentDescriptor.getAllelicStats()->nTriallelic
                << " - tetra: " << alignmentDescriptor.getAllelicStats()->nTetrallelic << "\n";
    }

    std::string strSites = "sites";
    if (!cloUseAllSites) {
        strSites = "polymorphic sites";
    }
    Interface::instance()
            << endl
            << "Number of " << strSites << " where there are no gaps :  "
            << alignmentDescriptor.getAllelicStats()->nNoGap << "/" << alignment.activeLength()
            << "\n";

    if (!cloUseAllSites) {
        Interface::instance()
                << endl
                << "Watterson's Theta            :  "
                << alignment.activeLength() / logl(alignment.countUsedSequences()) << "\n"
                << "Watterson's Theta (per site) :  "
                << alignment.activeLength()
                   / (logl(alignment.countUsedSequences()) * alignment.nCoveredNuPositions())
                << "\n";
    }

    if (cloUseAllSites) {
        Interface::instance()
                << endl
                << "Min  pairwise-distance between strains :  "
                << alignmentDescriptor.getPwDistanceStats()->minPwDist << "\n"
                << "Max  pairwise-distance between strains :  "
                << alignmentDescriptor.getPwDistanceStats()->maxPwDist << "\n"
                << "Mean pairwise-distance between strains :  "
                << alignmentDescriptor.getPwDistanceStats()->meanPwDist << "\n"
                << "Mean pairwise-distance per site (pi)   :  "
                << alignmentDescriptor.getPwDistanceStats()->meanPwDistPerSite << "\n";
    } else {
        Interface::instance()
                << endl
                << "Min  pairwise-distance between strains   :  "
                << alignmentDescriptor.getPwDistanceStats()->minPwDist << "\n"
                << "Max  pairwise-distance between strains   :  "
                << alignmentDescriptor.getPwDistanceStats()->maxPwDist << "\n"
                << "Mean pairwise-distance between strains   :  "
                << alignmentDescriptor.getPwDistanceStats()->meanPwDist << "\n"
                << "Mean pairwise-dist. per polymorphic site :  "
                << alignmentDescriptor.getPwDistanceStats()->meanPwDistPerSite << "\n";
    }
    Interface::instance().showOutput(true);

    Interface::instance()
            << "Notes:" << "\n"
            << "    AA-- and --AA are distinct but the pairwise-distance between them is zero.\n";

    Interface::instance().showLog(true);
}