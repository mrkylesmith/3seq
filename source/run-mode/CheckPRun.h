/* 
 * File:   TestPRun.h
 * Author: Maciej F. Boni, Ha Minh Lam
 *
 * Created on 21 August 2011, 17:39
 */

#ifndef CHECKPRUN_H
#define    CHECKPRUN_H

#include <cstdint> // for detecting the OS architecture through __WORDSIZE
#include <cassert>
#include "Run.h"
#include "../stat/PTableFile.h"

class CheckPRun : public Run {
public:
    struct MNK {
        long m;
        long n;
        long k;
    };

    explicit CheckPRun(int argc, char **argv);

    ~CheckPRun() override;

    bool isLogFileSupported() const override {
        return false;
    };

    Mode getMode() const override {
        return CHECK_P_VALUES;
    };

    void parseCmdLine() override;

    void perform() override;

    CheckPRun(const CheckPRun &orig) = delete;

    CheckPRun &operator=(const CheckPRun &rhs) = delete;


private:

#if __WORDSIZE == 64   // 64-bit
    /** The maximum number of digits that M, N, K can have. */
    static const unsigned int MAX_NUM_LENGTH = 9;

#else   // 32-bit
    /** The maximum number of digits that M, N, K can have. */
    static const unsigned int MAX_NUM_LENGTH = 4;
#endif

    /** Path to P-value table file */
    PTableFile *pTableFile;

};

#endif    /* CHECKPRUN_H */

