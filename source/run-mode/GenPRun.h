/* 
 * File:   GenPRun.h
 * Author: Maciej F. Boni, Ha Minh Lam
 *
 * Created on 19 August 2011, 17:54
 */

#ifndef GENPRUN_H
#define	GENPRUN_H

#include <cassert>
#include "Run.h"

class GenPRun : public Run {
public:
    GenPRun(const GenPRun& orig) = delete;

    GenPRun& operator=(const GenPRun& rhs) = delete;

    explicit GenPRun(int argc, char** argv);

    ~GenPRun() override = default;

    bool isLogFileSupported() const override {
        return false;
    };

    Mode getMode() const override {
        return GEN_P_TABLE;
    };

    void parseCmdLine() override;

    void perform() override;


private:
    string pTableFilePath;
    
    int pTableSize;
};

#endif	/* GENPRUN_H */

