/* 
 * File:   InfoRun.h
 * Author: Maciej F. Boni, Ha Minh Lam
 *
 * Created on 18 August 2011, 17:50
 */

#ifndef INFO_RUN_H
#define	INFO_RUN_H

#include <cassert>
#include "Run.h"
#include "../file/SequenceFile.h"

class InfoRun : public Run {
public:
    explicit InfoRun(int argc, char** argv);

    ~InfoRun() override;

    bool isLogFileSupported() const override {
        return true;
    };

    Mode getMode() const override {
        return INFO;
    };

    void parseCmdLine() override;

    void perform() override;


    InfoRun(const InfoRun& orig) = delete;

    InfoRun& operator=(const InfoRun& rhs) = delete;

};

#endif	/* INFO_RUN_H */

