//
// Created by lamhm on 14/03/2022.
//

#ifndef INC_3SEQ_NUMERIC_TYPES_H
#define INC_3SEQ_NUMERIC_TYPES_H

#include <limits>
#include <cmath>


namespace Long {
    typedef long Type;
    const auto NOT_SET = std::numeric_limits<Type>::max();
    const auto MAX = std::numeric_limits<Type>::max() - 1;
}

namespace ULong {
    typedef unsigned long Type;
    const auto NOT_SET = std::numeric_limits<Type>::max();
    const auto MAX = std::numeric_limits<Type>::max() - 1;
}

namespace LLong {
    typedef long long Type;
    const Type NOT_SET = std::numeric_limits<Type>::max();
    const Type MAX = std::numeric_limits<Type>::max() - 1;
}

namespace Double {
    typedef double Type;
    const Type NOT_SET = std::numeric_limits<Type>::quiet_NaN();
    const Type MAX = std::numeric_limits<Type>::max();
}

namespace Float {
    typedef float Type;
    const Type NOT_SET = std::numeric_limits<Type>::quiet_NaN();
    const Type MAX = std::numeric_limits<Type>::max();
}


inline bool isSet(Long::Type numericVar) {
    return numericVar != Long::NOT_SET;
}


inline bool isSet(ULong::Type numericVar) {
    return numericVar != ULong::NOT_SET;
}


inline bool isSet(LLong::Type numericVar) {
    return numericVar != LLong::NOT_SET;
}


inline bool isSet(Double::Type numericVar) {
    return !std::isnan(numericVar);
}


inline bool isSet(Float::Type numericVar) {
    return !std::isnan(numericVar);
}


#endif //INC_3SEQ_NUMERIC_TYPES_H
