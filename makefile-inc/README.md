# File Descriptions

| #   | File                     | Content                                                    |
|-----|:-------------------------|:-----------------------------------------------------------|
| 1   | Project-variables.mk     | project-specific variables                                 |
| 2   | Project-preprocessors.mk | project-specific macro that will be passed to the compiler |
| 3   | Makefile-variables.mk    | compiler settings                                          |
| 4   | Makefile-help.mk         | text to be printed out when executing `make help`          |

**Notes:** In most case, you do not need to modify anything except the variables in the first 2
files above. Adding variables is acceptable. Changing variables' names is very error-prone.

