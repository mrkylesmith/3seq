# 3SEQ Recombination Detection Algorithm

## Overview

3SEQ is a command-line program that reads in a nucleotide sequence file, in phylip or aligned fasta
format, and tests all sequence triplets in the file for a mosaic recombination signal indicating
that one of the three sequences (the child) is a recombinant of the other two (the parents). The
statistical test used is a [non-parametric test for mosaicism](http://mol.ax/delta) whose p-values
should be pre-computed once with the 3SEQ executable.

For bug reports or questions, please feel free to email Maciej Boni at `mfb9@psu.edu` or Hạ Minh Lâm
at `ha.minhlam@gmail.com`.

More information on how to compile and use 3SEQ can be found [here](http://mol.ax/3seq).

## Slowly-evolving Manual

Download the manual by clicking
on [this link](http://mol.ax/content/media/2018/02/3seq_manual.20180209.pdf).

## How to Cite 3SEQ

When using 3SEQ please cite

> **Lam HM, Ratmann O, Boni MF**.
> Improved algorithmic complexity for the 3SEQ recombination detection algorithm.<br>
> *Molecular Biology and Evolution*, 35:247–251, 2018.
> doi:[`10.1093/molbev/msx263`](https://doi.org/10.1093/molbev/msx263)

When referring to certain core parts of the statistics used, you may want to cite

> **Boni MF, Posada D, Feldman MW.**
> An exact nonparametric method for inferring mosaic structure in sequence triplets.<br>
> *Genetics*, 176:1035-1047, 2007.
> doi:[`10.1534/genetics.106.068874`](https://doi.org/10.1534/genetics.106.068874)

If you are making considerable use of the Hogan-Siegmund approximations, you should cite

> **Hogan ML, Siegmund D.**
> Large deviations for the maxima of some random fields.<br>
> *Advances in Applied Mathematics*, 7:2-22, 1986.
> doi:[`10.1016/0196-8858(86)90003-5`](https://doi.org/10.1016/0196-8858(86)90003-5)